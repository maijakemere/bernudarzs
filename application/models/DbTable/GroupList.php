<?php

class Application_Model_DbTable_GroupList extends Zend_Db_Table_Abstract
{

    protected $_name = 'group_list';
    protected $_referenceMap = array(
        'Childs' => array(
            'columns'           => array('child_id'),
            'refTableClass'     => 'Childs',
            'refColumns'        => array('id'),
            'onDelete'          => self::CASCADE,
            'onUpdate'          => self::RESTRICT
        )
    );

}

