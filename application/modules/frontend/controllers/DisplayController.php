<?php

class Frontend_DisplayController extends Zend_Controller_Action
{

    public function init()
    {
        $db = new Application_Model_DbTable_Userinfo();
        $selectUserinfo = $db->select();
        $rows = $db->fetchAll($selectUserinfo);

        $infoArray[] = null;
        foreach($rows as $user)
        {
            $infoArray[$user->userid] = $user;
        }
        $this->view->allUsers = $infoArray;
    }

    public function indexAction()
    {
        // action body
    }

    public function profileAction()
    {
        $id = $this->getRequest()->getParam('id');
        $db = new Frontend_Model_UserInfo();
        $userInfo = $db->profileInfo($id);

        $this->view->userInfo = $userInfo;
    }


}



