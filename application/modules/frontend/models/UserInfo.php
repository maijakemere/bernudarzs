<?php

class Frontend_Model_UserInfo
{

    public function profileInfo($id)
    {
        $db = new Application_Model_DbTable_Userinfo();
        $selectUserinfo = $db->select();
        $selectUserinfo -> where('userid = ' . $id);
        $rows = $db->fetchAll($selectUserinfo);

        if($rows != null){
            return $rows;
        } else {
            return null;
        }
    }

    public function allUsers()
    {
        $db = new Application_Model_DbTable_Userinfo();
        $selectUserinfo = $db->select();
        $rows = $db->fetchAll($selectUserinfo);

        $infoArray[] = null;
        foreach($rows as $user)
        {
            $infoArray[$user->userid] = $user;
        }
        return $infoArray;
    }

}

