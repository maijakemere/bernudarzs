<?php

class Log_Model_LogActions
{

    public function insertChildLogData($data){
        $logDb = new Application_Model_DbTable_ChildLog();
        $logDb->insert($data);
    }

    public function insertAuthenticationLogData($data){
        $logDb = new Application_Model_DbTable_AuthenticationLog();
        $logDb->insert($data);
    }

    //Vajag DB tabulu izveidot!
    public function insertClassLogData($data){
        $logDb = new Application_Model_DbTable_ClassLog();
        $logDb->insert($data);
    }

    public function insertStructureLogData($data){
        $logDb = new Application_Model_DbTable_StructureLog();
        $logDb->insert($data);
    }

    public function insertUserLogData($data){
        $logDb = new Application_Model_DbTable_UserLog();
        $logDb->insert($data);
    }

}

