<?php

class Class_Model_ClassInfo
{

    /**
     * Creates new class entry
     *
     * @param $info array - array of inputs for database
     */
    public function registerClass($info)
    {

        $db = new Application_Model_DbTable_Class();

        $data = array(
            'id' 	    => '',
            'name'	    => $info['name'],
            'date'	    => $info['date'],
            'time_from'	=> $info['time_from'],
            'time_till' => $info['time_till'],
            'group'     => $info['group'],
            'teacher'	=> $info['teacher'],
            'workflow'	=> $info['workflow']
        );

        $db->insert($data);

        $userObject = Zend_Auth::getInstance()->getIdentity();
        $logDb = new Application_Model_DbTable_UserLog();
        $time = Zend_Date::now()->toString('yyyy-MM-dd HH:mm:ss');;

        $logText = 'CLASS id'.''.' name '.$info['name'].' date '.$info['date'].
            ' time_from '.$info['time_from'].
            ' time_till '.$info['time_till'].
            ' group '.$info['group'].
            ' teacher '.$info['teacher'].
            ' workflow '.$info['workflow'];

        $logData = array(
            'id'               => '',
            'user_id' 	       => $userObject->userid,
            'action'           => '[REGISTERED]',
            'message'          => $logText,
            'timestamp'        => $time
        );

        $logDb->insert($logData);

    }

    /**
     * Lists all registered classes
     *
     * @return null|Zend_Db_Table_Rowset_Abstract
     *
     * Returns list of all classes or null if no registered classes
     */
    public function listAllClasses()
    {
        $db = new Application_Model_DbTable_Class();
        $selectClasses = $db->select();
        $selectClasses->order('id ASC');
        $rows = $db->fetchAll($selectClasses);

        if($rows != null){
            return $rows;
        } else {
            return null;
        }

    }

    /**
     * Selects all info about class
     *
     * @param $id string - class id
     * @return null|Zend_Db_Table_Rowset_Abstract
     *
     * Returns class or null if no classes match
     */
    public function getClassInfo($id)
    {
        $db = new Application_Model_DbTable_Class();

        $select = $db->select()
            ->from(array('c' => 'class'),
                array('*'))
            ->joinLeft(array('i' => 'userinfo'),
                'c.teacher = i.userid',
                array('firstname', 'lastname') )
            ->joinLeft(array('g' => 'group'),
                'c.group = g.id',
                array('id') );
        $select->setIntegrityCheck(false);
        $select->where('c.id = '.$id);
        $rows = $db->fetchAll($select);

        if($rows != null){
            return $rows;
        } else {
            return null;
        }

    }

    /**
     * Selects all info about class and returns as array
     *
     * @param $id string - class id
     * @return array|null
     *
     * Returns array containing class info or null if no classes match
     */
    public function getClassInfoArr($id)
    {
        $db = new Application_Model_DbTable_Class();

        $selectClass= $db->select();
        $selectClass->where('id = '.$id);
        $rows = $db->fetchAll($selectClass);

        if($rows != null){
            return $rows->toArray();
        } else {
            return null;
        }

    }

    /**
     * Updates class information
     *
     * @param $id string - class id
     * @param $diff array - array of changed objects
     */
    public function updateClass($id, $diff)
    {
        $userObject = Zend_Auth::getInstance()->getIdentity();
        $logDb = new Application_Model_DbTable_StructureLog();
        $time = Zend_Date::now()->toString('yyyy-MM-dd HH:mm:ss');;

        $logText = 'CLASS';
        foreach($diff as $key => $dif)
        {
            $logText .=''.$key.':'.$dif;
        }

        $logData = array(
            'id'               => '',
            'user_id' 	       => $userObject->userid,
            'action'           => '[UPDATED]',
            'message'          => $logText,
            'timestamp'        => $time
        );

        $logDb->insert($logData);
        $db = new Application_Model_DbTable_Class();
        $where = 'id = '.$id;
        $db->update($diff, $where);
    }

    /**
     * Selects all child from group
     *
     * @param $group string - id of group
     * @return null|Zend_Db_Table_Rowset_Abstract
     *
     * Returns list of all children registered in group or null if no registered children
     */
    public function selectGroupChildren($group)
    {
        $db = new Application_Model_DbTable_Childs();

        $selectChildren= $db->select()
            ->from('childs',
                array('*'))
            ->where('group = '.$group);
                   /* $selectChildren->where('group = '.$group);
        $rows = $db->fetchAll($selectChildren)*/
        $rows = $selectChildren;

         if($rows != null){
            return $rows;
         } else {
             return null;
         }
    }

    /**
     * Deletes class
     *
     * @param $id string - class id
     */
    public function deleteClass($id)
    {
        $db = new Application_Model_DbTable_Class();
        $db->delete('id = '.$id);
    }

}

