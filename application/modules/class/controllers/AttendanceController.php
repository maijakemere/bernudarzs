<?php
/**
 * Class Class_AttendanceController
 *
 * Handles attendance info
 */

class Class_AttendanceController extends Zend_Controller_Action
{
    /**
     * Iniatilize
     */
    public function init()
    {
        /* Initialize action controller here */
    }

    /**
     * Default action
     */
    public function indexAction()
    {
        // action body
    }

    /**
     * Register new class
     */
    public function registerAction()
    {
        $userObject = Zend_Auth::getInstance()->getIdentity();

        $params = $this->getRequest()->getParams();
        if (isset($params['id'])) {
            $id= $this->getRequest()->getParam('id');

            $info = new Class_Model_ClassInfo();
            $infoarr = $info->getClassInfoArr($id);
            $dataArr = $infoarr[0];
            $group = $dataArr['group'];
            $children = $info->selectGroupChildren($group);

            $this->view->children = $children;

        } else {
            //$this->listAction();
        }



    }

    /**
     * Edit class
     */
    public function editAction()
    {
        // action body
    }

    /*
    * Update class
    */
    public function updateAction()
    {
        // action body
    }


}







