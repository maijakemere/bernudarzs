<?php
/**
 * Class Class_ClassController
 *
 * Handles all class information
 *
 */









class Class_ClassController extends Zend_Controller_Action
{

    /**
     * Initialize function
     *
     */
    public function init()
    {
        /* Initialize action controller here */
    }

    /**
     * Default action
     *
     */
    public function indexAction()
    {
        // action body
    }

    /**
     * Register new class
     *
     */
    public function registerAction()
    {
        $userObject = Zend_Auth::getInstance()->getIdentity();
        echo $userObject->userid;
        $form = new Application_Form_CreateClass();
        $request = $this->getRequest();
        if($request->isPost())
        {

            if($form->isValid($this->_request->getPost()))
            {

                $name = $form->getValue('name');
                $date = $form->getValue('date');
                $timefrom = $form->getValue('timefrom');
                $timetill = $form->getValue('timetill');
                $group = $form->getValue('group');
                $workflow = $form->getValue('workflow');


                if(!empty($name) && !empty($date) && !empty($timefrom) && !empty($timetill))
                {

                    $infoArray = array (
                        'name'    => $name,
                        'date' => $date,
                        'time_from'   => $timefrom,
                        'time_till'   => $timetill,
                        'group'  => $group,
                        'teacher'   => $userObject->userid,
                        'workflow'  => $workflow
                    );

                    $accounts = new Class_Model_ClassInfo();

                    $accounts->registerClass($infoArray);

                    $this->view->successMessage = 'Reģistrācija veiksmīga!';

                }
                else
                {
                    $this->view->errorMessage = 'Jāaizpilda visi obligātie lauki!';
                }

            }
        }
        $this->view->form = $form;
    }

    /**
     * Edit class
     *
     */
    public function editAction()
    {
        $params = $this->getRequest()->getParams();
        if (isset($params['id'])) {
            $id= $this->getRequest()->getParam('id');

            $form = new Application_Form_EditClass();
            $info = new Class_Model_ClassInfo();
            $infoarr = $info->getClassInfoArr($id);
            $dataArr = $infoarr[0];

            $form->populate($dataArr);
            $this->view->form = $form;

        } else {
            $this->listAction();
        }
    }

    /**
     * Update class action
     *
     */
    public function updateAction()
    {
        $request = $this->getRequest();
        if($request->isPost())
        {
            $data = $this->_request->getPost();

            $id = $data['id'];
            $info = new Class_Model_ClassInfo();
            $infoarr = $info->getClassInfoArr($id);
            $dataArr = $infoarr[0];
            unset($dataArr['teacher']);


            $diff= array_diff_assoc($data,$dataArr);
            unset($diff['submit']);

            if(!empty($diff)) {
                $child = new Class_Model_ClassInfo();
                $child->updateClass($id, $diff);
                $this->view->successMessage = 'Bērna dati veiksmīgi laboti!';
            } else {
                $this->view->errorMessage = 'Nav ievadīti maināmi dati';
            }
        }
    }

    /**
     * List all classes
     *
     */
    public function listAction()
    {
        $classList = new  Class_Model_ClassInfo();
        $classes = $classList->listAllClasses();

        $this->view->class = $classes;
    }

    /**
     * View individual classes
     *
     */
    public function viewAction()
    {
        $id= $this->getRequest()->getParam('id');
        $class = new Class_Model_ClassInfo();
        $classInfo = $class->getClassInfo($id);

        $this->view->classInfo = $classInfo;
    }

    /**
     * Delete class
     */
    public function deleteAction()
    {
        $params = $this->getRequest()->getParams();
        if (isset($params['id'])) {
            $id = $this->getRequest()->getParam('id');
            $child = new Class_Model_ClassInfo();
            $child->deleteClass($id);
        } else {
            $this->listAction();
        }
    }


}

