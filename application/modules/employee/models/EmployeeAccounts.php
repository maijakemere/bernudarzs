<?php
/**
 * Class Employee_Model_EmployeeAccounts
 *
 * Handles employee accounts
 */

class Employee_Model_EmployeeAccounts
{

    /**
     * Registers new user
     *
     * @param $info array - information about employee
     */
    public function registerUser($info)
    {
        $db = new Application_Model_DbTable_Users();

        $data = array(
            'userid' 	=> '',
            'username'	=> $info['username'],
            'password'	=> $info['password'],
            'email'		=> $info['email']
        );

        $db->insert($data);

        $rows = $db->fetchAll('username = "'.$info['username'].'"');
        $userid = $rows->current()->userid;

        $db = new Application_Model_DbTable_Userinfo();

        $data = array(
            'userid' 		=> $userid,
            'firstname'		=> $info['name'],
            'lastname'		=> $info['surname'],
            'pers_code' => $info['perscode'],
            'phone'     => $info['phone'],
            'address'   => $info['address']
        );

        $db->insert($data);

        $db = new Application_Model_DbTable_Employment();

        $data = array(
            'employee_id' 		=> $userid,
            'position'		=> $info['position']
        );

        $db->insert($data);

        $db = new Application_Model_DbTable_UserPermissions();

        $data = array(
            'user_id' 		=> $userid,
            'permission_id'		=> $info['permission']
        );

        $db->insert($data);

        $userObject = Zend_Auth::getInstance()->getIdentity();
        $logDb = new Application_Model_DbTable_UserLog();
        $time = Zend_Date::now()->toString('yyyy-MM-dd HH:mm:ss');;

        $logText = 'userid' .$userid.' firstname '.$info['name'].' lastname '.$info['surname'].
            ' pers_code '.$info['perscode'].' phone '.$info['phone'].' address '.$info['address'];

        $logData = array(
            'id'               => '',
            'user_id' 	       => $userObject->userid,
            'edited_user_id'   => $userid,
            'action'           => '[REGISTERED]',
            'message'          => $logText,
            'timestamp'        => $time
        );

        $logDb->insert($logData);
    }

    /**
     * Updates user information
     *
     * @param $id string - employee id
     * @param $diff array - difference array
     */
    public function updateEmployee($id, $diff){

        $userObject = Zend_Auth::getInstance()->getIdentity();
        $logDb = new Application_Model_DbTable_UserLog();
        $time = Zend_Date::now()->toString('yyyy-MM-dd HH:mm:ss');;

        $logText = '';
        foreach($diff as $key => $dif)
        {
            $logText .=''.$key.':'.$dif;
        }

        $logData = array(
            'id'               => '',
            'user_id' 	       => $userObject->userid,
            'edited_user_id'   => $id,
            'action'           => '[UPDATED]',
            'message'          => $logText,
            'timestamp'        => $time
        );

        $logDb->insert($logData);

        $userarr = array();

        if(isset($diff['position'])) {
            $posarr = array ('position' => $diff['position']);
            unset($diff['position']);
        }

        if(isset($diff['permission'])) {
            $permarr = array ('permission_id' => $diff['permission']);
            unset($diff['permission']);
        }

        if(isset($diff['username'])) {
            $userarr['username'] = $diff['username'];
            unset($diff['username']);
        }

        if(isset($diff['email'])) {
            $userarr['email'] = $diff['email'];
            unset($diff['email']);
        }

        if(!empty($diff)){
            $db = new Application_Model_DbTable_Userinfo();
            $where = 'userid = '.$id;
            $db->update($diff, $where);
        }

        if(!empty($userarr)){
            $db = new Application_Model_DbTable_Users();
            $where = 'userid = '.$id;
            $db->update($userarr, $where);
        }

        if(!empty($posarr)){
            $db = new Application_Model_DbTable_Employment();
            $where = 'employee_id = '.$id;
            $db->update($posarr, $where);
        }

        if(!empty($permarr)){
            $db = new Application_Model_DbTable_UserPermissions();
            $where = 'user_id = '.$id;
            $db->update($permarr, $where);
        }

    }

    /**
     * Deletes employee
     *
     * @param $id string - employee id
     */
    public function deleteEmployee($id)
    {
        $db = new Application_Model_DbTable_Users();
        $db->delete('id = '.$id);
    }

}

