<?php
/**
 * Class Employee_Model_GetEmployeeInfo
 *
 * Handles employee information
 */

class Employee_Model_GetEmployeeInfo
{
    /**
     * Gets employee list
     *
     * @return null|Zend_Db_Table_Rowset_Abstract
     */
    public function listAllEmployees()
    {
        $db = new Application_Model_DbTable_Userinfo();
        $selectChilds = $db->select();
        $selectChilds->order('lastname ASC');
        $rows = $db->fetchAll($selectChilds);

        if($rows != null){
            return $rows;
        } else {
            return null;
        }

    }

    /**
     * Get employee information
     *
     * @param $id string - employee id
     * @return null|Zend_Db_Table_Rowset_Abstract - array of information or null
     */
    public function getEmployeeInfo($id)
    {
        $db = new Application_Model_DbTable_Userinfo();

        $select = $db->select()
            ->from(array('u' => 'users'),
                array('*'))
            ->join(array('i' => 'userinfo'),
                'u.userid = i.userid',
                array('*') );
        $select->setIntegrityCheck(false);
        $select->where('u.userid = '.$id);
        $rows = $db->fetchAll($select);

        if($rows != null){
            return $rows;
        } else {
            return null;
        }

    }

    /**
     * Gets employee information in array
     *
     * @param $id string - employee id
     * @return array|null - returns employee information
     */
    public function getEmployeeInfoArr($id)
    {
        $db = new Application_Model_DbTable_Userinfo();

        $select = $db->select()
            ->from(array('u' => 'users'),
                array('*'))
            ->joinLeft(array('i' => 'userinfo'),
                'u.userid = i.userid',
                array('*') )
            ->joinLeft(array('p' => 'user_permissions'),
                'p.user_id = i.userid',
                array('*') )
            ->joinLeft(array('e' => 'employment'),
                'e.employee_id = i.userid',
                array('*') );
        $select->setIntegrityCheck(false);
        $select->where('u.userid = '.$id);
        $rows = $db->fetchAll($select);

        if($rows != null){
            return $rows->toArray();
        } else {
            return null;
        }

    }

}

