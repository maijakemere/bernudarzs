<?php
/**
 * Class Employee_EmployeeController
 *
 * Handles employee information
 */

class Employee_EmployeeController extends Zend_Controller_Action
{
    /**
     * Initialize
     */
    public function init()
    {
        /* Initialize action controller here */
    }

    /**
     * Default action
     */
    public function indexAction()
    {
        // action body
    }

    /**
     * Register new employee
     */
    public function registerAction()
    {
        $form = new Application_Form_RegisterEmployee();

        $request = $this->getRequest();
        if($request->isPost())
        {
            if($form->isValid($this->_request->getPost()))
            {
                var_dump($this->_request->getPost());
                $username = $form->getValue('username');
                $password = $form->getValue('password');
                $repeatedPassword = $form->getValue('passwordRepeated');
                $email = $form->getValue('email');
                $name = $form->getValue('name');
                $surname = $form->getValue('surname');
                $position = $form->getValue('position');
                $permission = $form->getValue('permission');
                $perscode = $form->getValue('perscode');
                $phone = $form->getValue('phone');
                $address = $form->getValue('adress');
                echo "nav 1";
                if(!empty($username) && !empty($password) && !empty($repeatedPassword) && !empty($email) &&
                    !empty($name) && !empty($surname) && !empty($position) && !empty($permission) &&
                    !empty($perscode) && !empty($phone) && !empty($address))
                {
                    echo "nav 2";
                    $userinfo = new Authorize_Model_GetUserInfo();
                    if($userinfo->checkIfExists($username))
                    {
                        $this->view->errorMessage = 'Šāds lietotājs jau eksistē';
                    }

                    elseif($password != $repeatedPassword)
                    {
                        $this->view->errorMessage = 'Atkārtotā parole ir nepareiza';
                    }

                    else
                    {
                        $info = array(
                            'username' 		=> $username,
                            'password' 		=> md5($password),
                            'email'			=> $email,
                            'name' 	        => $name,
                            'surname' 		=> $surname,
                            'position'      => $position,
                            'permission'    => $permission,
                            'perscode'      => $perscode,
                            'phone'         => $phone,
                            'address'       => $address
                        );

                        $accounts = new Employee_Model_EmployeeAccounts();
                        $accounts->registerUser($info);

                        echo 'Reģistrācija veiksmīga! <br />';
                    }
                }
                else
                {
                    echo 'Jāaizpilda visi obligātie lauki.';
                }
            }
        }
        $this->view->form = $form;
    }

    /**
     * Edit employee action
     */
    public function editAction()
    {
        $params = $this->getRequest()->getParams();
        if (isset($params['id'])) {
            $id= $this->getRequest()->getParam('id');

            $form = new Application_Form_EditEmployee();
            $info = new Employee_Model_GetEmployeeInfo();
            $infoarr = $info->getEmployeeInfoArr($id);
            $dataArr = $infoarr[0];
            $form->populate($dataArr);
            $this->view->form = $form;

        } else {
            $this->listAction();
        }
    }

    /**
     * Delete employee action
     */
    public function deleteAction()
    {
        $params = $this->getRequest()->getParams();
        if (isset($params['id'])) {
            $id = $this->getRequest()->getParam('id');
            $child = new Employee_Model_EmployeeAccounts();
            $child->deleteEmployee($id);
        } else {
            $this->listAction();
        }
    }

    /**
     * List all employees
     */
    public function listAction()
    {
        $employeeList = new  Employee_Model_GetEmployeeInfo();
        $employees = $employeeList->listAllEmployees();

        $this->view->employee = $employees;
    }

    /**
     * View individual employee account
     */
    public function viewAction()
    {
        $id= $this->getRequest()->getParam('id');
        $employee = new Employee_Model_GetEmployeeInfo();
        $employeeInfo = $employee->getEmployeeInfo($id);

        $this->view->employeeInfo = $employeeInfo;
    }

    /**
     * Update employee account
     */
    public function updateAction()
    {
        $request = $this->getRequest();
        if($request->isPost())
        {
            $data = $this->_request->getPost();
            $id = $data['userid'];
            $info = new Employee_Model_GetEmployeeInfo();
            $infoarr = $info->getEmployeeInfoArr($id);
            $dataArr = $infoarr[0];

            $diff= array_diff_assoc($data,$dataArr);
            unset($diff['submit']);

            if(!empty($diff)) {
                $child = new Employee_Model_EmployeeAccounts();
                $child->updateEmployee($id, $diff);
                $this->view->successMessage = 'Darbinieka dati veiksmīgi laboti!';

            } else {
                $this->view->errorMessage = 'Nav ievadīti maināmi dati';
            }
        }
    }


}













