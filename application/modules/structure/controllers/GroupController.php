<?php
/**
 * Class Structure_GroupController
 *
 * Handles group information
 */

class Structure_GroupController extends Zend_Controller_Action
{
    /**
     * Initialize
     */
    public function init()
    {
        /* Initialize action controller here */
    }

    /**
     * Default action
     */
    public function indexAction()
    {
        // action body
    }

    /**
     * Register new group
     */
    public function registerAction()
    {
        $form = new Application_Form_RegisterGroup();
        $request = $this->getRequest();
        if($request->isPost())
        {

            if($form->isValid($this->_request->getPost()))
            {

                $name = $form->getValue('name');
                $description = $form->getValue('description');

                if(!empty($name))
                {
                    $groupinfo = new Structure_Model_GroupInfo();

                    if($groupinfo->checkIfExists($name))
                    {
                        $this->view->errorMessage = 'Šāda grupa jau ir reģistrēta sistēmā';
                    }

                    else
                    {

                        $groupArray = array (
                            'name'    => $name,
                            'description' => $description
                        );

                        $groupinfo->registerGroup($groupArray);

                        $this->view->successMessage = 'Grupas reģistrācija veiksmīga!';
                    }
                }
                else
                {
                    $this->view->errorMessage = 'Jāaizpilda visi obligātie lauki!';
                }
            }
        }
        $this->view->form = $form;
    }

    /**
     * Edit existing group
     */
    public function editAction()
    {
        $params = $this->getRequest()->getParams();
        if (isset($params['id'])) {
            $id= $this->getRequest()->getParam('id');

            $form = new Application_Form_EditGroup();
            $info = new Structure_Model_GroupInfo();
            $infoarr = $info->getGroupInfoArr($id);
            $dataArr = $infoarr[0];
            $form->populate($dataArr);
            $this->view->form = $form;

        } else {
            $this->listAction();
        }
    }

    /**
     * Update exisisting group
     */
    public function updateAction()
    {
        $request = $this->getRequest();
        if($request->isPost())
        {
            $data = $this->_request->getPost();

            $id = $data['id'];
            $info = new Structure_Model_GroupInfo();
            $infoarr = $info->getGroupInfoArr($id);
            $dataArr = $infoarr[0];

            $diff= array_diff_assoc($data,$dataArr);
            unset($diff['submit']);

            if(!empty($diff)) {
                $child = new Structure_Model_GroupInfo();
                $child->updateGroup($id, $diff);
                $this->view->successMessage = 'Grupas veiksmīgi laboti!';
            } else {
                $this->view->errorMessage = 'Nav ievadīti maināmi dati';
            }
        }
    }

    /**
     * Delete existing group
     */
    public function deleteAction()
    {
        $params = $this->getRequest()->getParams();
        if (isset($params['id'])) {
            $id = $this->getRequest()->getParam('id');
            $child = new Structure_Model_GroupInfo();
            $child->deleteGroup($id);
        }
        else {
            $this->listAction();
        }
    }

    /**
     * List all registered groups
     */
    public function listAction()
    {
        $groupList = new  Structure_Model_GroupInfo();
        $groups = $groupList->listAllGroups();

        $this->view->group = $groups;
    }

    /**
     * View individual group info
     */
    public function viewAction()
    {
        $id= $this->getRequest()->getParam('id');
        $child = new Structure_Model_GroupInfo();
        $groupInfo = $child->getGroupInfo($id);

        $this->view->groupInfo = $groupInfo;
    }


}













