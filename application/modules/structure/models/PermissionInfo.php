<?php
/**
 * Class Structure_Model_PermissionInfo
 *
 * Handles permission information
 */

class Structure_Model_PermissionInfo
{
    /**
     * Gets permission pairs for form - ID => Name
     *
     * @return array - permission pairs
     */
    public function getPermissionPairs()
    {
        $db = new Application_Model_DbTable_Permissions();
        $select = $db->select()->from('permissions', array('id', 'name'));
        $result = $db->getDefaultAdapter()->fetchPairs($select);
        return $result;
    }

}

