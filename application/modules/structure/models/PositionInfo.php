<?php
/**
 * Class Structure_Model_PositionInfo
 *
 * Handles position information
 */
class Structure_Model_PositionInfo
{
    /**
     * Gets position pairs - ID => Name
     *
     * @return array - returns position pairs
     */
    public function getPositionPairs()
    {
        $db = new Application_Model_DbTable_Position();
        $select = $db->select()->from('position', array('id', 'name'));
        $result = $db->getDefaultAdapter()->fetchPairs($select);
        return $result;
    }

}

