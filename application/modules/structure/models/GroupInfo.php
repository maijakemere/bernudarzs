<?php
/**
 * Class Structure_Model_GroupInfo
 *
 * Uses group information
 */
class Structure_Model_GroupInfo
{
    /**
     * Gets group info pairs -> ID => Name
     *
     * @return array - group pairs
     */
    public function getGroupPairs()
    {
        $db = new Application_Model_DbTable_Group();
        $select = $db->select()->from('group', array('id', 'name'));
        $result = $db->getDefaultAdapter()->fetchPairs($select);
        return $result;
    }

    /**
     * Checks if group with specific name exists
     *
     * @param $name string - group name
     * @return bool - returns true if exists
     */
    public function checkIfExists($name)
    {
        $db = new Application_Model_DbTable_Group();
        $selectGroups = $db->select();
        $selectGroups->where('name = "'.$name.'"', 'NEW');

        $rows = $db->fetchAll($selectGroups);
        $groups = $rows->current();

        if($groups != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * List all registered groups
     *
     * @return null|Zend_Db_Table_Rowset_Abstract - returns group info or null if no groups
     */
    public function listAllGroups()
    {
        $db = new Application_Model_DbTable_Group();
        $selectGroups = $db->select();
        $selectGroups->order('id ASC');
        $rows = $db->fetchAll($selectGroups);

        if($rows != null){
            return $rows;
        } else {
            return null;
        }

    }

    /**
     * Gets specific group information
     *
     * @param $id string - group id
     * @return null|Zend_Db_Table_Rowset_Abstract - returns information if found or null if !exists
     */
    public function getGroupInfo($id)
    {
        $db = new Application_Model_DbTable_Group();
        $selectGroups = $db->select();
        $selectGroups->where('id = '.$id);

        $rows = $db->fetchAll($selectGroups);

        if($rows != null){
            return $rows;
        } else {
            return null;
        }

    }

    /**
     * Gets specific group information array
     *
     * @param $id string - group id
     * @return array|null - group information or null if not exisists
     */
    public function getGroupInfoArr($id)
    {
        $db = new Application_Model_DbTable_Group();
        $selectGroups = $db->select();
        $selectGroups->where('id = '.$id);

        $rows = $db->fetchAll($selectGroups);

        if($rows != null){
            return $rows->toArray();
        } else {
            return null;
        }

    }

    /**
     * Register new group
     *
     * @param $info array - new group information
     */
    public function registerGroup($info)
    {
        $db = new Application_Model_DbTable_Group();

        $data = array(
            'id' 	=> '',
            'name'	=> $info['name'],
            'description'	=> $info['description']
        );

        $db->insert($data);

        $userObject = Zend_Auth::getInstance()->getIdentity();
        $logDb = new Application_Model_DbTable_StructureLog();
        $time = Zend_Date::now()->toString('yyyy-MM-dd HH:mm:ss');;

        $logText = 'GROUP id'.''.
            ' name '.$info['name'].
            ' description '.$info['description'];

        $logData = array(
            'id'               => '',
            'user_id' 	       => $userObject->userid,
            'action'           => '[REGISTERED]',
            'message'          => $logText,
            'timestamp'        => $time
        );

        $logDb->insert($logData);

    }

    /**
     * Updates group information
     *
     * @param $id string - group id
     * @param $diff array - differences to be written in database
     */
    public function updateGroup($id, $diff){

        $userObject = Zend_Auth::getInstance()->getIdentity();
        $logDb = new Application_Model_DbTable_StructureLog();
        $time = Zend_Date::now()->toString('yyyy-MM-dd HH:mm:ss');;

        $logText = 'GROUP';
        foreach($diff as $key => $dif)
        {
            $logText .=''.$key.':'.$dif;
        }

        $logData = array(
            'id'               => '',
            'user_id' 	       => $userObject->userid,
            'action'           => '[UPDATED]',
            'message'          => $logText,
            'timestamp'        => $time
        );

        $logDb->insert($logData);

        $db = new Application_Model_DbTable_Group();
        $where = 'id = '.$id;
        $db->update($diff, $where);
    }

}

