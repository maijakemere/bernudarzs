<?php

/**
 * Class Authorize_AuthorizeController
 *
 * Controller for authorization
 */

class Authorize_AuthorizeController extends Zend_Controller_Action
{

    /**
     * Initialize controller
     */
    public function init()
    {
        /* Initialize action controller here */
    }

    /**
     * Default action
     */
    public function indexAction()
    {
        // action body
    }

    /**
     * Login action
     */
    public function loginAction()
    {
        if(Zend_Auth::getInstance()->hasIdentity())
        {
            $this->_redirect('/frontend/display');
        }

        $form = new Application_Form_LoginForm();

        $request = $this->getRequest();
        if($request->isPost())
        {
            if($form->isValid($this->_request->getPost()))
            {
                $authAdapter = $this->getAuthAdapter();

                $username = $form->getValue('username');
                $password = md5($form->getValue('password'));

                $authAdapter->setIdentity($username)
                    ->setCredential($password);

                $auth = Zend_Auth::getInstance();
                $result = $auth->authenticate($authAdapter);

                if($result->isValid())
                {
                    $userInfo = new Authorize_Model_GetUserInfo();

                    $user = $authAdapter->getResultRowObject();

                    $userInfo = $userInfo->getUserInfo($user->userid);

                    $session = new Authorize_Model_UserSession($user, $userInfo);

                    $authStorage = $auth->getStorage();
                    $authStorage->write($session);
                    $this->_redirect('/frontend/display');
                }
                else
                {
                    $this->view->errorMessage = 'Lietotājvārds vai parole ievadīti nepareizi';
                }
            }
        }

        $this->view->form = $form;
    }

    /**
     * Logaut action
     */
    public function logoutAction()
    {
        Zend_Auth::getInstance()->clearIdentity();
    }

    /**
     * Gets built in authorization adapter
     *
     * @return Zend_Auth_Adapter_DbTable
     */
    public function getAuthAdapter()
    {
        $authAdapter = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());

        $authAdapter->setTableName('users')
            ->setIdentityColumn('username')
            ->setCredentialColumn('password');

        return $authAdapter;
    }

    /**
     * New user registration action
     */
    public function registerAction()
    {
        if(Zend_Auth::getInstance()->hasIdentity())
        {
            $this->_redirect('/frontend/display');
        }

        $form = new Application_Form_Register();
        $request = $this->getRequest();
        if($request->isPost())
        {
            if($form->isValid($this->_request->getPost()))
            {

                $username = $form->getValue('username');
                $password = $form->getValue('password');
                $repeatedPassword = $form->getValue('passwordRepeated');
                $email = $form->getValue('email');
                $name = $form->getValue('name');
                $surname = $form->getValue('surname');

                if(!empty($username) && !empty($password) && !empty($repeatedPassword) && !empty($email) &&
                    !empty($name) && !empty($surname))
                {
                    $userinfo = new Authorize_Model_GetUserInfo();
                    if($userinfo->checkIfExists($username))
                    {
                        $this->view->errorMessage = 'Šāds lietotājs jau eksistē';
                    }

                    elseif($password != $repeatedPassword)
                    {
                        $this->view->errorMessage = 'Atkārtotā parole ir nepareiza';
                    }

                    else
                    {
                        $info = array(
                            'username' 		=> $username,
                            'password' 		=> md5($password),
                            'email'			=> $email,
                            'name' 	        => $name,
                            'surname' 		=> $surname
                        );

                        $accounts = new Authorize_Model_Accounts();
                        $accounts->registerUser($info);

                        echo 'Reģistrācija veiksmīga! <br />';
                    }
                }
                else
                {
                    echo 'Jāaizpilda visi obligātie lauki.';
                }
            }
        }
        $this->view->form = $form;

    }

}









