<?php

/**
 * Class Authorize_Model_Accounts
 *
 * Handles user authorization
 */

class Authorize_Model_Accounts
{
    /**
     * Registers new user
     *
     * @param $info array - user information
     */
    public function registerUser($info)
    {

        $db = new Application_Model_DbTable_Users();

        $data = array(
            'userid' 	=> '',
            'username'	=> $info['username'],
            'password'	=> $info['password'],
            'email'		=> $info['email']
        );

        $db->insert($data);

        $rows = $db->fetchAll('username = "'.$info['username'].'"');
        $userid = $rows->current()->userid;

        $db = new Application_Model_DbTable_Userinfo();

        $data = array(
            'userid' 		=> $userid,
            'firstname'		=> $info['name'],
            'lastname'		=> $info['surname']
        );

        $db->insert($data);
    }

    /**
     * Deletes user
     *
     * @param $id string - deletable user id
     */
    public function deleteUser($id)
    {
        $db = new Application_Model_DbTable_Users();
        $db->delete('id = '.$id);
    }

    /**
     * Upades user information
     *
     * @param $infoArray array - updatable information
     * @param $userid string - user id
     */
    public function updateUserInfo($infoArray, $userid)
    {
        $user = null;

        if(array_key_exists('email', $infoArray))
        {
            $db = new Application_Model_DbTable_Users();
            $data = array('email' => $infoArray['email']);
            $where = 'userid = '.$userid;

            $db->update($data, $where);
            unset($infoArray['email']);
        }

        $db = new Application_Model_DbTable_Userinfo();
        $where = 'userid = '.$userid;
        $db->update($infoArray, $where);
        $rows = $db->fetchAll($where);
        $userinfo = $rows[0];

        $db = new Application_Model_DbTable_Users();
        $rows = $db->fetchAll($where);
        $user = $rows[0];


        $userInfo = new Authorize_Model_GetUserInfo();
        $userInfo = $userInfo->getUserInfo($user->userid);
        $session = new Authorize_Model_UserSession($user, $userInfo);
        $authStorage = Zend_Auth::getInstance()->getStorage();
        $authStorage->write($session);
    }

    /**
     * Updates user information
     *
     * @param $infoArray array - updatable information
     * @param $userid string - user id
     */
    public function updateUser($infoArray, $userid)
    {
        $db = new Application_Model_DbTable_Users();
        $where = 'userid = '.$userid;
        $db->update($infoArray, $where);
    }


}

