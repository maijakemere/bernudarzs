<?php

class Authorize_Model_UserSession
{

    public $userid;
    public $username;
    public $email;
    public $firstname;
    public $lastname;

    public function __construct($userRow, $infoRow)
    {
        $this->userid = $userRow->userid;
        $this->username = $userRow->username;
        $this->email = $userRow->email;

        $this->firstname = $infoRow->firstname;
        $this->lastname = $infoRow->lastname;
    }

}

