<?php

class Children_Model_ChildrenAccounts
{
    /**
     * Registers new parent
     *
     * @param $info array
     * @return string returns last inserted id
     */
    public function registerParent($info)
    {

        $db = new Application_Model_DbTable_Parents();

        $data = array(
            'parent_id' 	    => '',
            'parent_name'	    => $info['parentName'],
            'parent_surname'    => $info['parentSurname'],
            'parent_phone'	    => $info['parentPhone'],
            'parent_email'	    => $info['parentEmail'],
            'parent_adress'	    => $info['parentAdress']
        );

        $db->insert($data);
        $lastInsertId = $db->getAdapter()->lastInsertId();

        return $lastInsertId;
    }

    /**
     * Registers new child
     *
     * @param $info array
     */
    public function registerChildren($info)
    {

        $db = new Application_Model_DbTable_Childs();

        $data = array(
            'id' 	    => '',
            'name'	    => $info['name'],
            'surname'	=> $info['surname'],
            'pers_code'	=> $info['persCode'],
            'birth_date'=> $info['birthDate'],
            'group'     => $info['group'],
            'father'	=> $info['father'],
            'mother'	=> $info['mother'],
            'foster'	=> $info['foster'],
            'adress'	=> $info['adress']
        );

        $db->insert($data);
        $lastInsertId = $db->getAdapter()->lastInsertId();

        $userObject = Zend_Auth::getInstance()->getIdentity();

        $logDb = new Application_Model_DbTable_ChildLog();
        $time = Zend_Date::now()->toString('yyyy-MM-dd HH:mm:ss');;

        $logText = 'Name:' . $info['name'] . ' Surname:' . $info['surname'] .
            ' Birth date:' . $info['birthDate'] . ' Gender:' . $info['gender'] . ' Group:' . $info['group'] .
            ' Father:' . $info['father'] . ' Mother:' . $info['mother'] . ' Adress: ' . $info['adress'];

        $logData = array(
            'id'            => '',
            'user' 	        => $userObject->userid,
            'child'         => $lastInsertId,
            'action'        => '[CREATED]',
            'text'          => $logText,
            'time'          => $time
        );

        $logDb->insert($logData);
    }

    /**
     * Registers child in group
     *
     * @param $info array - group and child id
     */
    public function registerInGroup($info)
    {
        $db = new Application_Model_DbTable_GroupList();
        $userObject = Zend_Auth::getInstance()->getIdentity();
        $logDb = new Application_Model_DbTable_ChildLog();
        $time = Zend_Date::now()->toString('yyyy-MM-dd HH:mm:ss');;

        $groupData = array(
            'child_id'  => $info['childId'],
            'group_id'  => $info['groupId']
        );

        $db->insert($groupData);

        $logText = 'Registered child with id ' . $info['childId'] . ' in group ' . $info['groupId'];

        $logData = array(
            'id'            => '',
            'user' 	        => $userObject->userid,
            'child'         => $info['childId'],
            'action'        => '[UPDATED]',
            'text'          => $logText,
            'time'          => $time
        );

        $logDb->insert($logData);
    }

    /**
     * Deletes child
     * !IMPORTANT - MUST WRITE LOG UPDATE
     *
     * @param $id string - deletable child ir
     */
    public function deleteChild($id)
    {
        $db = new Application_Model_DbTable_Childs();
        $db->delete('id = '.$id);
        /*$db = new Application_Model_DbTable_Childs();
        $logDb = new Application_Model_DbTable_ChildLog();
        $userObject = Zend_Auth::getInstance()->getIdentity();
        $time = Zend_Date::now()->toString('yyyy-MM-dd HH:mm:ss');;

        $selectChild = $db->select();
        $selectChild->where('id = '.$id);
        foreach($selectChild as $row)
        {
            $rowClone = unserialize($row);
        }
        $logText = 'Id' . $rowClone->id . 'Name:' . $rowClone->name . ' Surname:' . $rowClone->surname . ' Birth date:' . $rowClone->birth_date . ' Gender:' . $rowClone->gender . ' Father:' . $rowClone->father . ' Mother:' . $rowClone->mother . ' Adress: ' . $rowClone->adress;

        $logData = array(
            'id'            => '',
            'user' 	        => $userObject->userid,
            'child'         => $id,
            'action'        => '[DELETED]',
            'text'          => $logText,
            'time'          => $time
        );

        $db->delete($id);*/
    }

    /**
     * Updates child info
     *
     * @param $id string - children id
     * @param $diff array - difference array
     */
    public function updateChild($id, $diff){
        $db = new Application_Model_DbTable_Childs();
        $where = 'id = '.$id;
        $db->update($diff, $where);

        $userObject = Zend_Auth::getInstance()->getIdentity();

        $logDb = new Application_Model_DbTable_ChildLog();
        $time = Zend_Date::now()->toString('yyyy-MM-dd HH:mm:ss');;

        $logText = '';
        foreach($diff as $key => $dif)
        {
            $logText .=''.$key.':'.$dif;
        }

        $logData = array(
            'id'            => '',
            'user' 	        => $userObject->userid,
            'child'         => $id,
            'action'        => '[UPDATED]',
            'text'          => $logText,
            'time'          => $time
        );

        $logDb->insert($logData);
    }

}

