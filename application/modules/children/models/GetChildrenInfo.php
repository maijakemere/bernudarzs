<?php

class Children_Model_GetChildrenInfo
{
    /**
     * Checks if child with person`s code isn`t allready registered
     *
     * @param $persCode string - persons code
     * @return bool - returns if children exists
     */
    public function checkIfExists($persCode)
    {
        $db = new Application_Model_DbTable_Childs();
        $selectChilds = $db->select();
        $selectChilds->where('pers_code = "'.$persCode.'"', 'NEW');

        $rows = $db->fetchAll($selectChilds);
        $childs = $rows->current();

        if($childs != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Validates person`s code by checksum
     *
     * @param $persCode string - persons code
     * @return bool - returns if valid code
     */
    public function validatePersCode($persCode)
    {
        $pk = preg_replace('/\D/', '', $persCode);
        if(strlen($pk) != 11) return false;
        $calc = 1*$pk[0]+6*$pk[1]+3*$pk[2]+7*$pk[3]+9*$pk[4]+10*$pk[5]+5*$pk[6]+8*$pk[7]+4*$pk[8]+2*$pk[9];
        $checksum = (1101-$calc)%11;
        return $checksum == $pk[10];
    }

    /**
     * Gets child info
     *
     * @param $id string - child id
     * @return null|Zend_Db_Table_Rowset_Abstract - returns data or null
     */
    public function getChildInfo($id)
    {
        $db = new Application_Model_DbTable_Childs();
        $groupDb = new Application_Model_DbTable_GroupList();
        $selectChild = $db->select();
        $selectChild->where('id = '.$id);
        // Atrast pareizu sintaksi JOIN
                   // ->join('group_list'. ON .'child_id='. $id);
        $rows = $db->fetchAll($selectChild);

        if($rows != null){
            return $rows;
        } else {
            return null;
        }

    }

    /**
     * Gets child info
     *
     * @param $id string - child id
     * @return array|null - returns child data in array or null if not existst
     */
    public function getChildInfoArr($id)
    {
        $db = new Application_Model_DbTable_Childs();
        $selectChild = $db->select();
        $selectChild->where('id = '.$id);
        $rows = $db->fetchAll($selectChild);

        if($rows != null){
            return $rows->toArray();
        } else {
            return null;
        }

    }

    /**
     * Lists all child data
     *
     * @return null|Zend_Db_Table_Rowset_Abstract - returns data about all child
     */
    public function listAllChild()
    {
        $db = new Application_Model_DbTable_Childs();
        $selectChilds = $db->select();
        $selectChilds->order('surname ASC');
        $rows = $db->fetchAll($selectChilds);

        if($rows != null){
            return $rows;
        } else {
            return null;
        }

    }

    /**
     * Gets group pairs for form
     *
     * @return array - group pairs (id => name)
     */
    public function getGroupPairs()
    {
        $db = new Application_Model_DbTable_Group();
        $select = $db->select()->from('group', array('id', 'name'));
        $result = $db->getDefaultAdapter()->fetchPairs($select);
        return $result;
    }

}

