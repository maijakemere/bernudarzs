<?php

/**
 * Class Children_Model_ListChild
 *
 * RANDOM CLASS MUST DELETE THIS ONE
 * BUT FIRST CHECK IF IT`S NOT USED
 */

class Children_Model_ListChild
{
    public function checkIfExists($persCode)
    {
        $db = new Application_Model_DbTable_Childs();
        $selectChilds = $db->select();
        $selectChilds->where('pers_code = "'.$persCode.'"', 'NEW');

        $rows = $db->fetchAll($selectChilds);
        $childs = $rows->current();

        if($childs != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private function getChildInfo($sqlString, $endString)
    {
        $db = new Application_Model_DbTable_Childs();
        $selectChilds = $db->select();
        $selectChilds->where($sqlString, $endString);

        return $db->fetchAll($selectChilds);
    }

    public function getChild($childid)
    {
        $db = new Application_Model_DbTable_Childs();
        $selectChild = $db->select()->where('id = '.$childid, 'NEW');

        return $db->fetchRow($selectChild);
    }

    public function getChildPics($childId)
    {

        $db = new Application_Model_DbTable_ChildPicture();
        $selectPics = $db->select();
        $selectPics->where('id = ?', $childId);

        return $db->fetchAll($selectPics);

    }

    public function addChild($name, $surname, $birthDate, $gender, $father, $mother, $adress, $picture)
    {
        $db = new Application_Model_DbTable_Childs();
        //$picDb = new Application_Model_DbTable_ChildPicture();
        $logDb = new Application_Model_DbTable_ChildLog();
        $userObject = Zend_Auth::getInstance()->getIdentity();

        $time = time();

        $data = array(
            'id' 	        => '',
            'name' 	        => $name,
            'surname'	    => $surname,
            'pers_code'     => $persCode,
            'birth_date'    => $birthDate,
            'gender'	    => $gender,
            'father'	    => $father,
            'mother'	    => $mother,
            'adress'	    => $adress
        );

        $db->insert($data);

        $lastInsertId = $this->getAdapter()->lastInsertId();

        $pictureData = array (
            'id' => $lastInsertId,
            'picture'   => $picture
        );

        $picDb->insert($pictureData);

        $logText = 'Name:' . $name . ' Surname:' . $surname . ' Birth date:' . $birthDate . ' Gender:' . $gender . ' Father:' . $father . ' Mother:' . $mother . ' Adress: ' . $adress;

        $logData = array(
            'id'            => '',
            'user' 	        => $userObject->userid,
            'child'         => $lastInsertId,
            'action'        => '[CREATED]',
            'text'          => $logText,
            'time'          => $time
        );

        $logDb->insert($logData);

        return true;
    }

    /*PABEIGT!!!!!!!!!!!!!!!!!!*/
    public function deleteChild($childId)
    {
        $db = new Application_Model_DbTable_Childs();
        $logDb = new Application_Model_DbTable_ChildLog();
        $picDb = new Application_Model_DbTable_ChildPicture();
        $userObject = Zend_Auth::getInstance()->getIdentity();

        $time = time();

        $row = $db->fetchRow('id = ' . $childId);
        $rowClone = unserialize($row);

        $logText = 'Id' . $rowClone->id . 'Name:' . $rowClone->name . ' Surname:' . $rowClone->surname . ' Birth date:' . $rowClone->birth_date . ' Gender:' . $rowClone->gender . ' Father:' . $rowClone->father . ' Mother:' . $rowClone->mother . ' Adress: ' . $rowClone->adress;

        $logData = array(
            'id'            => '',
            'user' 	        => $userObject->userid,
            'child'         => $rowClone->id,
            'action'        => '[DELETED]',
            'text'          => $logText,
            'time'          => $time
        );

        $logDb->insert($logData);



        // DELETE this row
        $row->delete();


    }

}

