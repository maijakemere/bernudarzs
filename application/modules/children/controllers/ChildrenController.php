<?php

class Children_ChildrenController extends Zend_Controller_Action
{

    /**
     * Initialize controller
     *
     */
    public function init()
    {
    }

    /**
     * Default controller action
     *
     */
    public function indexAction()
    {
    }

    /**
     * Register new child action
     *
     */
    public function registerAction()
    {
        $form = new Application_Form_RegisterChild();
        $request = $this->getRequest();
        if($request->isPost())
        {

            if($form->isValid($this->_request->getPost()))
            {

                $name = $form->getValue('name');
                $surname = $form->getValue('surname');
                $persCode = $form->getValue('persCode');
                $birthdate = $form->getValue('birthdate');
                $group = $form->getValue('group');
                $gender = $form->getValue('gender');
                $adress = $form->getValue('adress');
                $motherName = $form->getValue('motherName');
                $motherSurname = $form->getValue('motherSurname');
                $motherEmail = $form->getValue('motherEmail');
                $motherPhone = $form->getValue('motherPhone');
                $motherAdress = $form->getValue('motherAdress');
                $fatherName = $form->getValue('fatherName');
                $fatherSurname = $form->getValue('fatherSurname');
                $fatherPhone = $form->getValue('fatherPhone');
                $fatherEmail = $form->getValue('fatherEmail');
                $fatherAdress = $form->getValue('fatherAdress');
                $fosterName = $form->getValue('fosterName');
                $fosterSurname = $form->getValue('fosterSurname');
                $fosterPhone = $form->getValue('fosterPhone');
                $fosterEmail = $form->getValue('fosterEmail');
                $fosterAdress = $form->getValue('fosterAdress');

                if(!empty($name) && !empty($surname) && !empty($persCode) /*&& !empty($birthdate) &&
                    !empty($gender)*/ && !empty($adress))
                {
                    $childinfo = new Children_Model_GetChildrenInfo();

                    //Check if child is registered in system
                    if($childinfo->checkIfExists($persCode))
                    {
                        $this->view->errorMessage = 'Šāds bērns jau ir reģistrēts sistēmā';
                    }

                    //Check if person`s code is valid
                    elseif(!$childinfo->validatePersCode($persCode))
                    {
                        $this->view->errorMessage = 'Nav ievadīts derīgs personas kods';
                    }

                    else
                    {
                        //A child may have only mother or father or both or only foster
                        if (!empty($motherName) && !empty($motherSurname) && !empty($motherEmail) &&
                            !empty($motherPhone) && !empty($motherAdress))
                        {
                            $motherArray = array (
                                'parentName'    => $motherName,
                                'parentSurname' => $motherSurname,
                                'parentEmail'   => $motherEmail,
                                'parentPhone'   => $motherPhone,
                                'parentAdress'  => $motherAdress
                            );
                        }

                        if (!empty($fatherName) && !empty($fatherSurname) && !empty($fatherEmail) &&
                            !empty($fatherPhone) && !empty($fatherAdress))
                        {
                            $fatherArray = array (
                                'parentName'    => $fatherName,
                                'parentSurname' => $fatherSurname,
                                'parentEmail'   => $fatherEmail,
                                'parentPhone'   => $fatherPhone,
                                'parentAdress'  => $fatherAdress
                            );
                        }

                        if (!empty($fosterName) && !empty($fosterSurname) && !empty($fosterEmail) &&
                            !empty($fosterPhone) && !empty($fosterAdress))
                        {
                            $fosterArray = array (
                                'parentName'    => $fosterName,
                                'parentSurname' => $fosterSurname,
                                'parentEmail'   => $fosterEmail,
                                'parentPhone'   => $fosterPhone,
                                'parentAdress'  => $fosterAdress
                            );
                        }

                        //Register all parents
                        $accounts = new Children_Model_ChildrenAccounts();

                        $mother = $father = $foster = 0;

                        if (isset($motherArray))
                        {
                            $mother = $accounts->registerParent($motherArray);
                        }

                        if (isset($fatherArray))
                        {
                            $father = $accounts->registerParent($fatherArray);
                        }

                        if (isset($fosterArray))
                        {
                            $foster = $accounts->registerParent($fosterArray);
                        }

                        //Register child
                        $childArray = array(
                            'name'      => $name,
                            'surname'   => $surname,
                            'persCode'  => $persCode,
                            'birthDate' => $birthdate,
                            'group'     => $group,
                            'gender'    => $gender,
                            'father'    => $father,
                            'mother'    => $mother,
                            'foster'    => $foster,
                            'adress'    => $adress
                        );
                        $accounts->registerChildren($childArray);

                        $this->view->successMessage = 'Reģistrācija veiksmīga!';
                    }
                }
                else
                {
                    //If mandatory fields were blank, display error message (tripple check)
                    $this->view->errorMessage = 'Jāaizpilda visi obligātie lauki!';
                }
            }
        }
        //Display form
        $this->view->form = $form;
    }

    /**
     * List all children action
     *
     */
    public function listAction()
    {
        $childList = new  Children_Model_GetChildrenInfo;
        $childs = $childList->listAllChild();

        $this->view->child = $childs;
    }

    /**
     * View children profile action
     *
     */
    public function viewAction()
    {
        $id= $this->getRequest()->getParam('id');
        $child = new Children_Model_GetChildrenInfo();
        $childInfo = $child->getChildInfo($id);

        $this->view->childInfo = $childInfo;
    }

    /**
     * Edit child profile action
     *
     */
    public function editAction()
    {
        $params = $this->getRequest()->getParams();
        if (isset($params['id'])) {
            $id= $this->getRequest()->getParam('id');

            $form = new Application_Form_EditChild();
            $info = new Children_Model_GetChildrenInfo();
            $infoarr = $info->getChildInfoArr($id);
            $dataArr = $infoarr[0];
            $form->populate($dataArr);
            $this->view->form = $form;


        } else {
            $this->listAction();
        }
    }

    /**
     * Delete child profile action
     *
     */
    public function deleteAction()
    {
        $params = $this->getRequest()->getParams();
        if (isset($params['id'])) {
            $id = $this->getRequest()->getParam('id');
            $child = new Children_Model_ChildrenAccounts();
            $child->deleteChild($id);
        }
        else {
            $this->listAction();
        }
    }

    /**
     * Update child profile action
     *
     */
    public function updateAction()
    {
        $request = $this->getRequest();
        if($request->isPost())
        {
            $data = $this->_request->getPost();

            $id = $data['id'];
            $info = new Children_Model_GetChildrenInfo();
            $infoarr = $info->getChildInfoArr($id);
            $dataArr = $infoarr[0];

            $diff= array_diff_assoc($data,$dataArr);
            unset($diff['submit']);

            if(!empty($diff)) {
                $child = new Children_Model_ChildrenAccounts();
                $child->updateChild($id, $diff);
                $this->view->successMessage = 'Bērna dati veiksmīgi laboti!';
            } else {
                $this->view->errorMessage = 'Nav ievadīti maināmi dati';
            }
        }
    }

    public function addpictureAction()
    {
       /* $request = $this->getRequest();

        $form = new Application_Form_PictureUpload();

        // if POST data has been submitted
        if ($request->isPost()) {
            // if the UploadPicture form has been submitted and the submitted data is valid
            if (isset($_POST['upload_picture']) && $form->isValid($_POST)) {

                $data = $form->getValues();

                // get the picture extension
                $ext = end(explode('.', $form->picture->getFileName()));

                // you will probably want to save the picture in the database
                $dbTrans = false;

                try {

                    /*$db = $this->_getDb();
                    $dbTrans = $db->beginTransaction();

                    // query the database here

                    $db->commit();*/

                    // where do you want to save the image?
                    /*$path = 'images/' . $ext;

                    // add filter for renaming the uploaded picture
                    $form->picture->addFilter('Rename',
                        array('target' => $path,
                            'overwrite' => true));

                    // upload the picture
                    $form->picture->receive();

                    $form->reset();

                } catch (Exception $e) {
                    if (true === $dbTrans) {
                        $db->rollBack();
                    }
                    // pass possible exceptions to the view
                    $this->view->errorMessage = "Nevar saglabāt attēlu";
                }

            }
        }

        // pass the form to the view
        $this->view->form = $form;*/

    }


}



















