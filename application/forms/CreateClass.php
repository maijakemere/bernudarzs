<?php

/**
 * Class Application_Form_CreateClass
 *
 * Form for creating new class
 */

class Application_Form_CreateClass extends Zend_Form
{

    public function init()
    {

        // Gets data to display groups
        $groups = new Structure_Model_GroupInfo();
        $groupData = $groups->getGroupPairs();

        //Set form name, send option and action
        $this->setName('createclass');
        $this->setMethod('post');
        $this->setAction('./register');

        // Add a class name element
        $this->addElement('text', 'name', array(
            'label'      => 'Nosaukums:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array(
                    'validator' => 'StringLength',
                    'options'   => array(
                        'min'   => 2,
                        'max'   => 200,
                        'messages'  => array(
                            'stringLengthInvalid'   => 'Nosaukumam jābut starp %min% un %max% burtiem',
                            'stringLengthTooLong'   => 'Nosaukums nevar būt vairāk kā %max% burtus garš',
                            'stringLengthTooShort'  => 'Nosaukumam ir jābut vismaz %min% simbolus garam'
                        )
                    )
                ),
                array (
                    'validator' => 'NotEmpty' ,
                    'options'   => array(
                        'messages' => array(
                            'notEmptyInvalid'   => 'Ievadīts nepareizs datu tips',
                            'isEmpty'           => 'Lauks nedrīkst būt tukšs'
                        )
                    )
                )
            )
        ));

        // Add a class date element
        $date= new ZendX_JQuery_Form_Element_DatePicker('date');
        $date->setLabel('Datums:')
            ->setJQueryParam('dateFormat', 'dd.mm.yy')
            ->setJQueryParam('changeYear', 'true')
            ->setJqueryParam('changeMonth', 'true')
            ->setJqueryParam('regional', 'lv')
            ->setJqueryParam('yearRange', "2000:")
            ->addValidator(new Zend_Validate_Date(
                array(
                    'format'    => 'dd.mm.yyyy',
                    'options'   => array(
                        'messages'  => array(
                            'dateInvalid'       => "Nepareizs datu tips - sagaidāms, ka tu ievadi datumu",
                            'dateInvalidDate'   => "'%value%' nav pareizs datums",
                            'dateFalseFormat'   => "'%value%' neatbilst formātam '%format%'"
                        )
                    )
                )
            ))
            ->addValidator(new Zend_Validate_NotEmpty(
                array(
                    'options'   => array(
                        'messages' => array(
                            'notEmptyInvalid'   => 'Ievadīts nepareizs datu tips',
                            'isEmpty'           => 'Lauks nedrīkst būt tukšs'
                        )
                    )
                )
            ))

            ->setRequired(true);

        $this->addElement($date);

        // Add a class time from element
        $this->addElement('text', 'timefrom', array(
            'label'      => 'Laiks no:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array (
                    'validator' => 'NotEmpty' ,
                    'options'   => array(
                        'messages' => array(
                            'notEmptyInvalid'   => 'Ievadīts nepareizs datu tips',
                            'isEmpty'           => 'Lauks nedrīkst būt tukšs'
                        )
                    )
                ),
                array(
                    'validator' => 'Date',
                    'options'   => array(
                        'format' => 'H:i:s'
                    )
                )
            )
        ));

        $this->addElement('text', 'timetill', array(
            'label'      => 'Laiks līdz:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array (
                    'validator' => 'NotEmpty' ,
                    'options'   => array(
                        'messages' => array(
                            'notEmptyInvalid'   => 'Ievadīts nepareizs datu tips',
                            'isEmpty'           => 'Lauks nedrīkst būt tukšs'
                        )
                    )
                ),
                array(
                    'validator' => 'Date',
                    'options'   => array(
                        'format' => 'H:i:s'
                    )
                )
            )
        ));

        // Add a class group element
        $this->addElement('select', 'group', array(
            'label'         => 'Grupa:',
            'required'      => true,
            'multiOptions'  => $groupData
        ));

        // Add a class workflow element
        $this->addElement('textarea', 'workflow', array(
            'label'      => 'Paveiktais:',
            'required'   => false,
            'filters'    => array('StringTrim')
        ));

        // Add the submit button
        $this->addElement('submit', 'submit', array(
            'ignore'   => true,
            'label'    => 'Izveidot nodarbību',
            'class'      => 'btn btn-primary'
        ));

    }



}

