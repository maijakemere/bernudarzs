<?php

/**
 * Class Application_Form_EditEmployee
 *
 * Form for editing existing employee
 */

class Application_Form_EditEmployee extends Zend_Form
{

    public function init()
    {

        // Gets data to display positions
        $positions = new Structure_Model_PositionInfo();
        $positionData = $positions->getPositionPairs();

        // Gets data to display permissions
        $permissions = new Structure_Model_PermissionInfo();
        $permissionData = $permissions->getPermissionPairs();


        //Set form name, send option and action
        $this->setName('editemployee');
        $this->setMethod('post');
        $this->setAction('../../update');

        $this->addElement('hidden','userid', array(
        ));

        // Add an username element
        $this->addElement('text', 'username', array(
            'label'      => 'Lietotājvārds:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(6, 20))
            )
        ));

        // Add a password element
        /*$this->addElement('text', 'password', array(
            'label'      => 'Parole:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(6, 20))
            )
        ));

        // Add a repeated password element
        $this->addElement('text', 'passwordRepeated', array(
            'label'      => 'Parole atkārtoti:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(6, 20)),
                array('validator' => 'Identical', 'options' => array('token' => 'password'))
            )
        ));*/


        // Add an  email element
        $this->addElement('text', 'email', array(
            'label'      => 'E-pasts:',
            'filters'    => array('StringTrim'),
            'validators' => array(
                array(
                    'validator' => 'EmailAddress',
                    'options'   => array(
                        'messages'  => array(
                            'emailAddressInvalid'           => "Nepareizs e-pasta ievades tips",
                            'emailAddressInvalidFormat'     => "'%value%' nav e-pasts formātā nosaukums@domens.lv",
                            'emailAddressInvalidHostname'   => "'%hostname%' nav derīga e-pasta adreses '%value%' pirmā daļa",
                            'emailAddressInvalidMxRecord'   => "'%hostname%' nav derīga MX ieraksta adresē '%value%'",
                            'emailAddressInvalidSegment'    => "'%hostname%' is not in a routable network segment. The email address '%value%' should not be resolved from public network",
                            'emailAddressDotAtom'           => "'%localPart%' can not be matched against dot-atom format",
                            'emailAddressQuotedString'      => "'%localPart%' can not be matched against quoted-string format",
                            'emailAddressInvalidLocalPart'  => "'%localPart%' is no valid local part for email address '%value%'",
                            'emailAddressLengthExceeded'    => "'%value%' exceeds the allowed length"
                        )
                    )
                )
            )
        ));

        // Add a name element
        $this->addElement('text', 'firstname', array(
            'label'      => 'Vārds:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(2, 30))
            )
        ));

        // Add a surname element
        $this->addElement('text', 'lastname', array(
            'label'      => 'Uzvārds:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(2, 30))
            ),

        ));

        // Add a position element
        $this->addElement('select', 'position', array(
            'label'         => 'Amats',
            'required'      => true,
            'multiOptions'  => $positionData
        ));

        // Add a permission element
        $this->addElement('select', 'permission', array(
            'label'         => 'Tiesības',
            'required'      => true,
            'multiOptions'  => $permissionData
        ));

        // Add an employee personality code element
        $this->addElement('text', 'pers_code', array(
            'label'      => 'Personas kods:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array(
                    'validator' => 'StringLength',
                    'options'   => array(
                        'min'   => 12,
                        'max'   => 12,
                        'messages'  => array(
                            'stringLengthInvalid'   => 'Personas kodam ir jābut %max% simbolus garam',
                            'stringLengthTooLong'   => 'Personas kods nevar būt vairāk kā %max% simbolus garš',
                            'stringLengthTooShort'  => 'Personas kodam ir jābut %min% simbolus garam'
                        )
                    )
                ),
                array(
                    'validator' => 'regex',
                    'options'   => array(
                        'pattern'   => '/[0-9]{6}-[0-9]{5}/',
                        'messages'  =>  array(
                            'regexInvalid'  => "Nepareizs datu tips",
                            'regexNotMatch' => "'%value%' ievadītā vērtība neatbilst šablonam '%pattern%'",
                            'regexErrorous' => "Iekšējā kļūda, lietojot šablonu '%pattern%'",
                        )
                    )
                ),
                array (
                    'validator' => 'NotEmpty' ,
                    'options'   => array(
                        'messages' => array(
                            'notEmptyInvalid'   => 'Ievadīts nepareizs datu tips',
                            'isEmpty'           => 'Lauks nedrīkst būt tukšs'
                        )
                    )
                )
            )
        ));

        // Add a phone number element
        $this->addElement('text', 'phone', array(
            'label'      => 'Telefons: +371',
            'filters'    => array('StringTrim'),
            'validators' => array(
                array(
                    'validator' => 'StringLength',
                    'options'   => array(
                        'min'   => 8,
                        'max'   => 8,
                        'messages'  => array(
                            'stringLengthInvalid'   => 'Telefona Nr. var būt tikai %min% ciparus garš',
                            'stringLengthTooLong'   => 'Telefona Nr. nevar būt vairāk kā %max% ciparus garš',
                            'stringLengthTooShort'  => 'Telefona Nr. ir jābut %min% ciparus garam'
                        )
                    )
                ),
                array(
                    'validator' => 'Digits',
                    'messages'  => array(
                        'notDigits'         => "'%value%' telefona Nr. var saturēt tikai ciparus",
                        'digitsStringEmpty' => "'%value%' telefona Nr. lauks ir tukšs",
                        'digitsInvalid'     => "Ievadīts nepareizs datu tips"
                    )
                )
            )
        ));

        // Add a child adress element
        $this->addElement('text', 'address', array(
            'label'      => 'Adrese:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array(
                    'validator' => 'StringLength',
                    'options'   => array(
                        'min'   => 2,
                        'max'   => 250,
                        'messages'  => array(
                            'stringLengthInvalid'   => 'Adrese jābut starp %min% un %max% simboliem',
                            'stringLengthTooLong'   => 'Adrese nevar būt vairāk kā %max% simbolus gara',
                            'stringLengthTooShort'  => 'Adresei ir jābut vismaz %min% simbolus garai'
                        )
                    )
                ),
                array (
                    'validator' => 'NotEmpty' ,
                    'options'   => array(
                        'messages' => array(
                            'notEmptyInvalid'   => 'Ievadīts nepareizs datu tips',
                            'isEmpty'           => 'Lauks nedrīkst būt tukšs'
                        )
                    )
                )
            )
        ));

        // Add the submit button
        $this->addElement('submit', 'submit', array(
            'ignore'   => true,
            'label'    => 'Labot',
            'class'      => 'btn btn-primary'
        ));

    }


}

