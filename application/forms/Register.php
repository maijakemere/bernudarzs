<?php

/**
 * Class Application_Form_Register
 *
 * User registration form
 */

class Application_Form_Register extends Zend_Form
{

    public function init()
    {
        //Set form name, send option and action
        $this->setName('register');
        $this->setMethod('post');
        $this->setAction('./register');

        // Add an username element
        $this->addElement('text', 'username', array(
            'label'      => 'Lietotājvārds:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(6, 20))
            )
        ));

        // Add a password element
        $this->addElement('text', 'password', array(
            'label'      => 'Parole:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(6, 20))
            )
        ));

        // Add a repeated password element
        $this->addElement('text', 'passwordRepeated', array(
            'label'      => 'Parole atkārtoti:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(6, 20)),
                array('validator' => 'Identical', 'options' => array('token' => 'password'))
            )
        ));

        // Add an email element
        $this->addElement('text', 'email', array(
            'label'      => 'E-pasts:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                'EmailAddress',
            )
        ));

        // Add a name element
        $this->addElement('text', 'name', array(
            'label'      => 'Vārds:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(2, 30))
            )
        ));

        // Add a surname element
        $this->addElement('text', 'surname', array(
            'label'      => 'Uzvārds:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(2, 30))
            ),

        ));


        // Add a picture element
        /*$this->addElement('file', 'picture', array(
            'label'      => 'Attēls:',
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(6, 20))
            ),

        ));*/

        // Add the submit button
        $this->addElement('submit', 'submit', array(
            'ignore'   => true,
            'label'    => 'Reģistrēties',
            'class'      => 'btn btn-primary'
        ));

    }

}

