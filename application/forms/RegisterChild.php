<?php

/**
 * Class Application_Form_RegisterChild
 *
 * Child registration form
 */

class Application_Form_RegisterChild extends Zend_Form
{

    public function init()
    {

        // Gets data to display groups
        $groups = new Structure_Model_GroupInfo();
        $groupData = $groups->getGroupPairs();

        //Set form name, send option and action
        $this->setName('registerchild');
        $this->setMethod('post');
        $this->setAction('./register');

        // Add a child name element
        $this->addElement('text', 'name', array(
            'label'      => 'Vārds:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array(
                    'validator' => 'StringLength',
                    'options'   => array(
                        'min'   => 2,
                        'max'   => 30,
                        'messages'  => array(
                            'stringLengthInvalid'   => 'Vārdam jābut starp %min% un %max% burtiem',
                            'stringLengthTooLong'   => 'Vārds nevar būt vairāk kā %max% burtus garš',
                            'stringLengthTooShort'  => 'Vārdam ir jābut vismaz %min% simbolus garam'
                        )
                    )
                ),
                array(
                    'validator' => 'Alpha',
                    'options'   => array(
                        'allowWhitespace'   => 'true',
                        'messages'          => array(
                            'alphaInvalid'      => "Tu vari ievadīt tikai alfabēta burtus",
                            'notAlpha'          => "'%value%' satur ne tikai burtus",
                            'alphaStringEmpty'  => "'%value%' lauks ir tukšs"
                        )
                    )
                ),
                array (
                    'validator' => 'NotEmpty' ,
                    'options'   => array(
                        'messages' => array(
                            'notEmptyInvalid'   => 'Ievadīts nepareizs datu tips',
                            'isEmpty'           => 'Lauks nedrīkst būt tukšs'
                        )
                    )
                )
            )
        ));

        // Add a child surname element
        $this->addElement('text', 'surname', array(
            'label'      => 'Uzvārds:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array(
                    'validator' => 'StringLength',
                    'options'   => array(
                        'min'   => 2,
                        'max'   => 30,
                        'messages'  => array(
                            'stringLengthInvalid'   => 'Uzvārdam jābut starp %min% un %max% burtiem',
                            'stringLengthTooLong'   => 'Uzvārds nevar būt vairāk kā %max% burtus garš',
                            'stringLengthTooShort'  => 'Uzvārdam ir jābut vismaz %min% simbolus garam'
                        )
                    )
                ),
                array(
                    'validator' => 'Alpha',
                    'options'   => array(
                        'allowWhitespace'   => 'true',
                        'messages'          => array(
                            'alphaInvalid'      => "Tu vari ievadīt tikai alfabēta burtus",
                            'notAlpha'          => "'%value%' satur ne tikai burtus",
                            'alphaStringEmpty'  => "'%value%' lauks ir tukšs"
                        )
                    )
                ),
                array (
                    'validator' => 'NotEmpty' ,
                    'options'   => array(
                        'messages' => array(
                            'notEmptyInvalid'   => 'Ievadīts nepareizs datu tips',
                            'isEmpty'           => 'Lauks nedrīkst būt tukšs'
                        )
                    )
                )
            )
        ));

        // Add a child personality code element
        $this->addElement('text', 'persCode', array(
            'label'      => 'Personas kods:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array(
                    'validator' => 'StringLength',
                    'options'   => array(
                        'min'   => 12,
                        'max'   => 12,
                        'messages'  => array(
                            'stringLengthInvalid'   => 'Personas kodam ir jābut %max% simbolus garam',
                            'stringLengthTooLong'   => 'Personas kods nevar būt vairāk kā %max% simbolus garš',
                            'stringLengthTooShort'  => 'Personas kodam ir jābut %min% simbolus garam'
                        )
                    )
                ),
                array(
                    'validator' => 'regex',
                    'options'   => array(
                        'pattern'   => '/[0-9]{6}-[0-9]{5}/',
                        'messages'  =>  array(
                            'regexInvalid'  => "Nepareizs datu tips",
                            'regexNotMatch' => "'%value%' ievadītā vērtība neatbilst šablonam '%pattern%'",
                            'regexErrorous' => "Iekšējā kļūda, lietojot šablonu '%pattern%'",
                        )
                    )
                ),
                array (
                    'validator' => 'NotEmpty' ,
                    'options'   => array(
                        'messages' => array(
                            'notEmptyInvalid'   => 'Ievadīts nepareizs datu tips',
                            'isEmpty'           => 'Lauks nedrīkst būt tukšs'
                        )
                    )
                )
            )
        ));

        // Add a child birth date element
        $birthdate = new ZendX_JQuery_Form_Element_DatePicker('birthdate');
        $birthdate->setLabel('Dzimšanas datums:')
            ->setJQueryParam('dateFormat', 'dd.mm.yy')
            ->setJQueryParam('changeYear', 'true')
            ->setJqueryParam('changeMonth', 'true')
            ->setJqueryParam('regional', 'lv')
            ->setJqueryParam('yearRange', "2000:")
            ->addValidator(new Zend_Validate_Date(
                array(
                    'format'    => 'dd.mm.yyyy',
                    'options'   => array(
                        'messages'  => array(
                            'dateInvalid'       => "Nepareizs datu tips - sagaidāms, ka tu ievadi datumu",
                            'dateInvalidDate'   => "'%value%' nav pareizs datums",
                            'dateFalseFormat'   => "'%value%' neatbilst formātam '%format%'"
                        )
                    )
                )
            ))
            ->addValidator(new Zend_Validate_NotEmpty(
                array(
                    'options'   => array(
                        'messages' => array(
                            'notEmptyInvalid'   => 'Ievadīts nepareizs datu tips',
                            'isEmpty'           => 'Lauks nedrīkst būt tukšs'
                        )
                    )
                )
            ))

            ->setRequired(true);

        $this->addElement($birthdate);

        // Add a child gender element
        $this->addElement('select', 'gender', array(
            'label'      => 'Dzimums:',
            'required'   => true,
            'multiOptions'  => array(
                '1'     => 'Sieviete',
                '0'     => 'Vīrietis'
            )
        ));

        // Add a child group element
        $this->addElement('select', 'group', array(
            'label'         => 'Grupa',
            'required'      => true,
            'multiOptions'  => $groupData
        ));

        // Add a child adress element
        $this->addElement('text', 'adress', array(
            'label'      => 'Bērna adrese:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array(
                    'validator' => 'StringLength',
                    'options'   => array(
                        'min'   => 2,
                        'max'   => 250,
                        'messages'  => array(
                            'stringLengthInvalid'   => 'Adrese jābut starp %min% un %max% simboliem',
                            'stringLengthTooLong'   => 'Adrese nevar būt vairāk kā %max% simbolus gara',
                            'stringLengthTooShort'  => 'Adresei ir jābut vismaz %min% simbolus garai'
                        )
                    )
                ),
                array (
                    'validator' => 'NotEmpty' ,
                    'options'   => array(
                        'messages' => array(
                            'notEmptyInvalid'   => 'Ievadīts nepareizs datu tips',
                            'isEmpty'           => 'Lauks nedrīkst būt tukšs'
                        )
                    )
                )
            )
        ));

        // Add a mother`s name element
        $this->addElement('text', 'motherName', array(
            'label'      => 'Mātes vārds:',
            'filters'    => array('StringTrim'),
            'validators' => array(
                array(
                    'validator' => 'StringLength',
                    'options'   => array(
                        'min'   => 2,
                        'max'   => 30,
                        'messages'  => array(
                            'stringLengthInvalid'   => 'Vārdam jābut starp %min% un %max% burtiem',
                            'stringLengthTooLong'   => 'Vārds nevar būt vairāk kā %max% burtus garš',
                            'stringLengthTooShort'  => 'Vārdam ir jābut vismaz %min% simbolus garam'
                        )
                    )
                ),
                array(
                    'validator' => 'Alpha',
                    'options'   => array(
                        'allowWhitespace'   => 'true',
                        'messages'          => array(
                            'alphaInvalid'      => "Tu vari ievadīt tikai alfabēta burtus",
                            'notAlpha'          => "'%value%' satur ne tikai burtus",
                            'alphaStringEmpty'  => "'%value%' lauks ir tukšs"
                        )
                    )
                )
            )
        ));

        // Add a mother`s surname element
        $this->addElement('text', 'motherSurname', array(
            'label'      => 'Mātes uzvārds:',
            'filters'    => array('StringTrim'),
            'validators' => array(
                array(
                    'validator' => 'StringLength',
                    'options'   => array(
                        'min'   => 2,
                        'max'   => 30,
                        'messages'  => array(
                            'stringLengthInvalid'   => 'Uzvārdam jābut starp %min% un %max% burtiem',
                            'stringLengthTooLong'   => 'Uzvārds nevar būt vairāk kā %max% burtus garš',
                            'stringLengthTooShort'  => 'Uzvārdam ir jābut vismaz %min% simbolus garam'
                        )
                    )
                ),
                array(
                    'validator' => 'Alpha',
                    'options'   => array(
                        'allowWhitespace'   => 'true',
                        'messages'          => array(
                            'alphaInvalid'      => "Tu vari ievadīt tikai alfabēta burtus",
                            'notAlpha'          => "'%value%' satur ne tikai burtus",
                            'alphaStringEmpty'  => "'%value%' lauks ir tukšs"
                        )
                    )
                )
            )
        ));

        // Add an mother`s email element
        $this->addElement('text', 'motherEmail', array(
            'label'      => 'Mātes e-pasts:',
            'filters'    => array('StringTrim'),
            'validators' => array(
                array(
                    'validator' => 'EmailAddress',
                    'options'   => array(
                        'messages'  => array(
                            'emailAddressInvalid'           => "Nepareizs e-pasta ievades tips",
                            'emailAddressInvalidFormat'     => "'%value%' nav e-pasts formātā nosaukums@domens.lv",
                            'emailAddressInvalidHostname'   => "'%hostname%' nav derīga e-pasta adreses '%value%' pirmā daļa",
                            'emailAddressInvalidMxRecord'   => "'%hostname%' nav derīga MX ieraksta adresē '%value%'",
                            'emailAddressInvalidSegment'    => "'%hostname%' is not in a routable network segment. The email address '%value%' should not be resolved from public network",
                            'emailAddressDotAtom'           => "'%localPart%' can not be matched against dot-atom format",
                            'emailAddressQuotedString'      => "'%localPart%' can not be matched against quoted-string format",
                            'emailAddressInvalidLocalPart'  => "'%localPart%' is no valid local part for email address '%value%'",
                            'emailAddressLengthExceeded'    => "'%value%' exceeds the allowed length"
                        )
                    )
                )
            )
        ));

        // Add a mother`s phone number element
        $this->addElement('text', 'motherPhone', array(
            'label'      => 'Mātes telefons: +371',
            'filters'    => array('StringTrim'),
            'validators' => array(
                array(
                    'validator' => 'StringLength',
                    'options'   => array(
                        'min'   => 8,
                        'max'   => 8,
                        'messages'  => array(
                            'stringLengthInvalid'   => 'Telefona Nr. var būt tikai %min% ciparus garš',
                            'stringLengthTooLong'   => 'Telefona Nr. nevar būt vairāk kā %max% ciparus garš',
                            'stringLengthTooShort'  => 'Telefona Nr. ir jābut %min% ciparus garam'
                        )
                    )
                ),
                array(
                    'validator' => 'Digits',
                    'messages'  => array(
                        'notDigits'         => "'%value%' telefona Nr. var saturēt tikai ciparus",
                        'digitsStringEmpty' => "'%value%' telefona Nr. lauks ir tukšs",
                        'digitsInvalid'     => "Ievadīts nepareizs datu tips"
                    )
                )
            )
        ));

        // Add a mother`s adress element
        $this->addElement('text', 'motherAdress', array(
            'label'      => 'Mātes adrese:',
            'filters'    => array('StringTrim'),
            'validators' => array(
                array(
                    'validator' => 'StringLength',
                    'options'   => array(
                        'min'   => 2,
                        'max'   => 250,
                        'messages'  => array(
                            'stringLengthInvalid'   => 'Adrese jābut starp %min% un %max% simboliem',
                            'stringLengthTooLong'   => 'Adrese nevar būt vairāk kā %max% simbolus gara',
                            'stringLengthTooShort'  => 'Adresei ir jābut vismaz %min% simbolus garai'
                        )
                    )
                )
            )
        ));

        // Add a father`s name element
        $this->addElement('text', 'fatherName', array(
            'label'      => 'Tēva vārds:',
            'filters'    => array('StringTrim'),
            'validators' => array(
                array(
                    'validator' => 'StringLength',
                    'options'   => array(
                        'min'       => 2,
                        'max'       => 30,
                        'messages'  => array(
                            'stringLengthInvalid'   => 'Vārdam jābut starp %min% un %max% burtiem',
                            'stringLengthTooLong'   => 'Vārds nevar būt vairāk kā %max% burtus garš',
                            'stringLengthTooShort'  => 'Vārdam ir jābut vismaz %min% simbolus garam'
                        )
                    )
                ),
                array(
                    'validator' => 'Alpha',
                    'options'   => array(
                        'allowWhitespace'   => 'true',
                        'messages'          => array(
                            'alphaInvalid'     => "Tu vari ievadīt tikai alfabēta burtus",
                            'notAlpha'         => "'%value%' satur ne tikai burtus",
                            'alphaStringEmpty' => "'%value%' lauks ir tukšs"
                        )
                    )
                )
            )

        ));

        // Add a father`s surname element
        $this->addElement('text', 'fatherSurname', array(
            'label'      => 'Tēva uzvārds:',
            'filters'    => array('StringTrim'),
            'validators' => array(
                array(
                    'validator' => 'StringLength',
                    'options'   => array(
                        'min'   => 2,
                        'max'   => 30,
                        'messages'  => array(
                            'stringLengthInvalid'   => 'Uzvārdam jābut starp %min% un %max% burtiem',
                            'stringLengthTooLong'   => 'Uzvārds nevar būt vairāk kā %max% burtus garš',
                            'stringLengthTooShort'  => 'Uzvārdam ir jābut vismaz %min% simbolus garam'
                        )
                    )
                ),
                array(
                    'validator' => 'Alpha',
                    'options'   => array(
                        'allowWhitespace'   => 'true',
                        'messages'          => array(
                            'alphaInvalid'      => "Tu vari ievadīt tikai alfabēta burtus",
                            'notAlpha'          => "'%value%' satur ne tikai burtus",
                            'alphaStringEmpty'  => "'%value%' lauks ir tukšs"
                        )
                    )
                )
            )
        ));



        // Add an father`s email element
        $this->addElement('text', 'fatherEmail', array(
            'label'      => 'Tēva e-pasts:',
            'filters'    => array('StringTrim'),
            'validators' => array(
                array(
                    'validator' => 'EmailAddress',
                    'options'   => array(
                        'messages'  => array(
                            'emailAddressInvalid'           => "Nepareizs e-pasta ievades tips",
                            'emailAddressInvalidFormat'     => "'%value%' nav e-pasts formātā nosaukums@domens.lv",
                            'emailAddressInvalidHostname'   => "'%hostname%' nav derīga e-pasta adreses '%value%' pirmā daļa",
                            'emailAddressInvalidMxRecord'   => "'%hostname%' nav derīga MX ieraksta adresē '%value%'",
                            'emailAddressInvalidSegment'    => "'%hostname%' is not in a routable network segment. The email address '%value%' should not be resolved from public network",
                            'emailAddressDotAtom'           => "'%localPart%' can not be matched against dot-atom format",
                            'emailAddressQuotedString'      => "'%localPart%' can not be matched against quoted-string format",
                            'emailAddressInvalidLocalPart'  => "'%localPart%' is no valid local part for email address '%value%'",
                            'emailAddressLengthExceeded'    => "'%value%' exceeds the allowed length"
                        )
                    )
                )
            )
        ));

        // Add a father`s phone element
        $this->addElement('text', 'fatherPhone', array(
            'label'      => 'Tēva telefons: +371',
            'filters'    => array('StringTrim'),
            'validators' => array(
                array(
                    'validator' => 'StringLength',
                    'options'   => array(
                        'min'   => 8,
                        'max'   => 8,
                        'messages'  => array(
                            'stringLengthInvalid'   => 'Telefona Nr. var būt tikai %min% ciparus garš',
                            'stringLengthTooLong'   => 'Telefona Nr. nevar būt vairāk kā %max% ciparus garš',
                            'stringLengthTooShort'  => 'Telefona Nr. ir jābut %min% ciparus garam'
                        )
                    )
                ),
                array(
                    'validator' => 'Digits',
                    'messages'  => array(
                        'notDigits'         => "'%value%' telefona Nr. var saturēt tikai ciparus",
                        'digitsStringEmpty' => "'%value%' telefona Nr. lauks ir tukšs",
                        'digitsInvalid'     => "Ievadīts nepareizs datu tips"
                    )
                )
            )
        ));

        // Add a father`s adress element
        $this->addElement('text', 'fatherAdress', array(
            'label'      => 'Tēva adrese:',
            'filters'    => array('StringTrim'),
            'validators' => array(
                array(
                    'validator' => 'StringLength',
                    'options'   => array(
                        'min'   => 2,
                        'max'   => 250,
                        'messages'  => array(
                            'stringLengthInvalid'   => 'Adrese jābut starp %min% un %max% simboliem',
                            'stringLengthTooLong'   => 'Adrese nevar būt vairāk kā %max% simbolus gara',
                            'stringLengthTooShort'  => 'Adresei ir jābut vismaz %min% simbolus garai'
                        )
                    )
                )
            )
        ));

        // Add a foster`s name element
        $this->addElement('text', 'fosterName', array(
            'label'      => 'Aizbildņa vārds:',
            'filters'    => array('StringTrim'),
            'validators' => array(
                array(
                    'validator' => 'StringLength',
                    'options'   => array(
                        'min'   => 2,
                        'max'   => 30,
                        'messages'  => array(
                            'stringLengthInvalid'   => 'Vārdam jābut starp %min% un %max% burtiem',
                            'stringLengthTooLong'   => 'Vārds nevar būt vairāk kā %max% burtus garš',
                            'stringLengthTooShort'  => 'Vārdam ir jābut vismaz %min% simbolus garam'
                        )
                    )
                ),
                array(
                    'validator' => 'Alpha',
                    'options'   => array(
                        'allowWhitespace'   => 'true',
                        'messages'          => array(
                            'alphaInvalid'      => "Tu vari ievadīt tikai alfabēta burtus",
                            'notAlpha'    => "'%value%' satur ne tikai burtus",
                            'alphaStringEmpty' => "'%value%' lauks ir tukšs"
                        )
                    )
                )
            )
        ));

        // Add a foster`s surname element
        $this->addElement('text', 'fosterSurname', array(
            'label'      => 'Aizbildņa uzvārds:',
            'filters'    => array('StringTrim'),
            'validators' => array(
                array(
                    'validator' => 'StringLength',
                    'options'   => array(
                        'min'   => 2,
                        'max'   => 30,
                        'messages'  => array(
                            'stringLengthInvalid'   => 'Uzvārdam jābut starp %min% un %max% burtiem',
                            'stringLengthTooLong'   => 'Uzvārds nevar būt vairāk kā %max% burtus garš',
                            'stringLengthTooShort'  => 'Uzvārdam ir jābut vismaz %min% simbolus garam'
                        )
                    )
                ),
                array(
                    'validator' => 'Alpha',
                    'options'   => array(
                        'allowWhitespace'   => 'true',
                        'messages'          => array(
                            'alphaInvalid'      => "Tu vari ievadīt tikai alfabēta burtus",
                            'notAlpha'          => "'%value%' satur ne tikai burtus",
                            'alphaStringEmpty'  => "'%value%' lauks ir tukšs"
                        )
                    )
                )
            )
        ));

        // Add an foster`s email element
        $this->addElement('text', 'fosterEmail', array(
            'label'      => 'Aizbildņa e-pasts:',
            'filters'    => array('StringTrim'),
            'validators' => array(
                array(
                    'validator' => 'EmailAddress',
                    'options'   => array(
                        'messages'  => array(
                            'emailAddressInvalid'           => "Nepareizs e-pasta ievades tips",
                            'emailAddressInvalidFormat'     => "'%value%' nav e-pasts formātā nosaukums@domens.lv",
                            'emailAddressInvalidHostname'   => "'%hostname%' nav derīga e-pasta adreses '%value%' pirmā daļa",
                            'emailAddressInvalidMxRecord'   => "'%hostname%' nav derīga MX ieraksta adresē '%value%'",
                            'emailAddressInvalidSegment'    => "'%hostname%' is not in a routable network segment. The email address '%value%' should not be resolved from public network",
                            'emailAddressDotAtom'           => "'%localPart%' can not be matched against dot-atom format",
                            'emailAddressQuotedString'      => "'%localPart%' can not be matched against quoted-string format",
                            'emailAddressInvalidLocalPart'  => "'%localPart%' is no valid local part for email address '%value%'",
                            'emailAddressLengthExceeded'    => "'%value%' exceeds the allowed length"
                        )
                    )
                )
            )
        ));

        // Add a foster`s phone element
        $this->addElement('text', 'fosterPhone', array(
            'label'      => 'Aizbildņa telefons: +371',
            'filters'    => array('StringTrim'),
            'validators' => array(
                array(
                    'validator' => 'StringLength',
                    'options'   => array(
                        'min'   => 8,
                        'max'   => 8,
                        'messages'  => array(
                            'stringLengthInvalid'   => 'Telefona Nr. var būt tikai %min% ciparus garš',
                            'stringLengthTooLong'   => 'Telefona Nr. nevar būt vairāk kā %max% ciparus garš',
                            'stringLengthTooShort'  => 'Telefona Nr. ir jābut %min% ciparus garam'
                        )
                    )
                ),
                array(
                    'validator' => 'Digits',
                    'messages'  => array(
                        'notDigits'         => "'%value%' telefona Nr. var saturēt tikai ciparus",
                        'digitsStringEmpty' => "'%value%' telefona Nr. lauks ir tukšs",
                        'digitsInvalid'     => "Ievadīts nepareizs datu tips"
                    )
                )
            )
        ));

        // Add a foster`s adress element
        $this->addElement('text', 'fosterAdress', array(
            'label'      => 'Aizbildņa adrese:',
            'filters'    => array('StringTrim'),
            'validators' => array(
                array(
                    'validator' => 'StringLength',
                    'options'   => array(
                        'min'   => 2,
                        'max'   => 250,
                        'messages'  => array(
                            'stringLengthInvalid'   => 'Adrese jābut starp %min% un %max% simboliem',
                            'stringLengthTooLong'   => 'Adrese nevar būt vairāk kā %max% simbolus gara',
                            'stringLengthTooShort'  => 'Adresei ir jābut vismaz %min% simbolus garai'
                        )
                    )
                )
            )
        ));

        // Add the submit button
        $this->addElement('submit', 'submit', array(
            'ignore'   => true,
            'label'    => 'Reģistrēt bērnu',
            'class'      => 'btn btn-primary'
        ));

    }

}

