<?php

/**
 * Class Application_Form_PictureUpload
 *
 * Form for picture uploac
 */

class Application_Form_PictureUpload extends Zend_Form
{

    public function init()
    {
        //Set form name, send option and action
        $this->setName('uploadpic');
        $this->setMethod('post');
        $this->setAction('./uploadpic');

        // Add a picture element
        $picture = new Zend_Form_Element_File('picture', array(
            'label' => 'Attēls',
            'required' => true,
            'MaxFileSize' => 2097152, // 2097152 bytes = 2 megabytes
            'validators' => array(
                array('Count', false, 1),
                array('Size', false, 2097152),
                array('Extension', false, 'gif,jpg,png'),
                array('ImageSize', false, array('minwidth' => 100,
                    'minheight' => 100,
                    'maxwidth' => 1000,
                    'maxheight' => 1000))
            )
        ));

        $this->addElement($picture);

        // Add the submit button
        $this->addElement('submit', 'submit', array(
            'ignore'   => true,
            'label'    => 'Saglabāt',
            'class'      => 'btn btn-primary'
        ));

    }


}

