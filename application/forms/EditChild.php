<?php

/**
 * Class Application_Form_EditChild
 *
 * Form for editing existing child info
 */

class Application_Form_EditChild extends Zend_Form
{

    public function init()
    {

        // Gets data to display groups
        $groups = new Structure_Model_GroupInfo();
        $groupData = $groups->getGroupPairs();

        //Set form name, send option and action
        $this->setName('editchild');
        $this->setMethod('post');
        $this->setAction('../../update');

        $this->addElement('hidden','id', array(
            'label' => 'Id'
        ));

        // Add a child name element
        $this->addElement('text', 'name', array(
            'label'      => 'Vārds:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array(
                    'validator' => 'StringLength',
                    'options'   => array(
                        'min'   => 2,
                        'max'   => 30,
                        'messages'  => array(
                            'stringLengthInvalid'   => 'Vārdam jābut starp %min% un %max% burtiem',
                            'stringLengthTooLong'   => 'Vārds nevar būt vairāk kā %max% burtus garš',
                            'stringLengthTooShort'  => 'Vārdam ir jābut vismaz %min% simbolus garam'
                        )
                    )
                ),
                array(
                    'validator' => 'Alpha',
                    'options'   => array(
                        'allowWhitespace'   => 'true',
                        'messages'          => array(
                            'alphaInvalid'      => "Tu vari ievadīt tikai alfabēta burtus",
                            'notAlpha'          => "'%value%' satur ne tikai burtus",
                            'alphaStringEmpty'  => "'%value%' lauks ir tukšs"
                        )
                    )
                ),
                array (
                    'validator' => 'NotEmpty' ,
                    'options'   => array(
                        'messages' => array(
                            'notEmptyInvalid'   => 'Ievadīts nepareizs datu tips',
                            'isEmpty'           => 'Lauks nedrīkst būt tukšs'
                        )
                    )
                )
            )
        ));

        // Add a child surname element
        $this->addElement('text', 'surname', array(
            'label'      => 'Uzvārds:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array(
                    'validator' => 'StringLength',
                    'options'   => array(
                        'min'   => 2,
                        'max'   => 30,
                        'messages'  => array(
                            'stringLengthInvalid'   => 'Uzvārdam jābut starp %min% un %max% burtiem',
                            'stringLengthTooLong'   => 'Uzvārds nevar būt vairāk kā %max% burtus garš',
                            'stringLengthTooShort'  => 'Uzvārdam ir jābut vismaz %min% simbolus garam'
                        )
                    )
                ),
                array(
                    'validator' => 'Alpha',
                    'options'   => array(
                        'allowWhitespace'   => 'true',
                        'messages'          => array(
                            'alphaInvalid'      => "Tu vari ievadīt tikai alfabēta burtus",
                            'notAlpha'          => "'%value%' satur ne tikai burtus",
                            'alphaStringEmpty'  => "'%value%' lauks ir tukšs"
                        )
                    )
                ),
                array (
                    'validator' => 'NotEmpty' ,
                    'options'   => array(
                        'messages' => array(
                            'notEmptyInvalid'   => 'Ievadīts nepareizs datu tips',
                            'isEmpty'           => 'Lauks nedrīkst būt tukšs'
                        )
                    )
                )
            )
        ));

        // Add a child personality code element
        $this->addElement('text', 'pers_code', array(
            'label'      => 'Personas kods:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array(
                    'validator' => 'StringLength',
                    'options'   => array(
                        'min'   => 12,
                        'max'   => 12,
                        'messages'  => array(
                            'stringLengthInvalid'   => 'Personas kodam ir jābut %max% simbolus garam',
                            'stringLengthTooLong'   => 'Personas kods nevar būt vairāk kā %max% simbolus garš',
                            'stringLengthTooShort'  => 'Personas kodam ir jābut %min% simbolus garam'
                        )
                    )
                ),
                array(
                    'validator' => 'regex',
                    'options'   => array(
                        'pattern'   => '/[0-9]{6}-[0-9]{5}/',
                        'messages'  =>  array(
                            'regexInvalid'  => "Nepareizs datu tips",
                            'regexNotMatch' => "'%value%' ievadītā vērtība neatbilst šablonam '%pattern%'",
                            'regexErrorous' => "Iekšējā kļūda, lietojot šablonu '%pattern%'",
                        )
                    )
                ),
                array (
                    'validator' => 'NotEmpty' ,
                    'options'   => array(
                        'messages' => array(
                            'notEmptyInvalid'   => 'Ievadīts nepareizs datu tips',
                            'isEmpty'           => 'Lauks nedrīkst būt tukšs'
                        )
                    )
                )
            )
        ));

        // Add a child birth date element
        $birthdate = new ZendX_JQuery_Form_Element_DatePicker('birth_date');
        $birthdate->setLabel('Dzimšanas datums:')
            ->setJQueryParam('dateFormat', 'dd.mm.yy')
            ->setJQueryParam('changeYear', 'true')
            ->setJqueryParam('changeMonth', 'true')
            ->setJqueryParam('regional', 'lv')
            ->setJqueryParam('yearRange', "2000:")
            ->addValidator(new Zend_Validate_Date(
                array(
                    'format'    => 'dd.mm.yyyy',
                    'options'   => array(
                        'messages'  => array(
                            'dateInvalid'       => "Nepareizs datu tips - sagaidāms, ka tu ievadi datumu",
                            'dateInvalidDate'   => "'%value%' nav pareizs datums",
                            'dateFalseFormat'   => "'%value%' neatbilst formātam '%format%'"
                        )
                    )
                )
            ))
            ->addValidator(new Zend_Validate_NotEmpty(
                array(
                    'options'   => array(
                        'messages' => array(
                            'notEmptyInvalid'   => 'Ievadīts nepareizs datu tips',
                            'isEmpty'           => 'Lauks nedrīkst būt tukšs'
                        )
                    )
                )
            ))

            ->setRequired(true);

        $this->addElement($birthdate);

        // Add a child gender element
        $this->addElement('select', 'gender', array(
            'label'      => 'Dzimums:',
            'required'   => true,
            'multiOptions'  => array(
                '1'     => 'Sieviete',
                '0'     => 'Vīrietis'
            )
        ));

        // Add a child group element
        $this->addElement('select', 'group', array(
            'label'         => 'Grupa',
            'required'      => true,
            'multiOptions'  => $groupData
        ));

        // Add a child adress element
        $this->addElement('text', 'adress', array(
            'label'      => 'Bērna adrese:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array(
                    'validator' => 'StringLength',
                    'options'   => array(
                        'min'   => 2,
                        'max'   => 250,
                        'messages'  => array(
                            'stringLengthInvalid'   => 'Adrese jābut starp %min% un %max% simboliem',
                            'stringLengthTooLong'   => 'Adrese nevar būt vairāk kā %max% simbolus gara',
                            'stringLengthTooShort'  => 'Adresei ir jābut vismaz %min% simbolus garai'
                        )
                    )
                ),
                array (
                    'validator' => 'NotEmpty' ,
                    'options'   => array(
                        'messages' => array(
                            'notEmptyInvalid'   => 'Ievadīts nepareizs datu tips',
                            'isEmpty'           => 'Lauks nedrīkst būt tukšs'
                        )
                    )
                )
            )
        ));

        // Add the submit button
        $this->addElement('submit', 'submit', array(
            'ignore'   => true,
            'label'    => 'Labot bērnu',
            'class'      => 'btn btn-primary'
        ));
    }

}

