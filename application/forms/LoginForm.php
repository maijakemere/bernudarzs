<?php

/**
 * Class Application_Form_LoginForm
 *
 * Form for login
 */

class Application_Form_LoginForm extends Zend_Form
{

    public function __construct($option = null)
    {
        parent::__construct($option);

        $this->setName('login');


        //username
        $username = new Zend_Form_Element_Text('username');
        $username->setLabel('Lietotājs: ')
            ->setRequired();

        //password
        $password = new Zend_Form_Element_Password('password');
        $password->setLabel('Parole: ')
            ->setRequired();

        //Login button
        $login = new Zend_Form_Element_Submit('login');
        $login->setLabel('Pieteikties')
            ->class = 'btn btn-primary';

        $this->addElements(array($username, $password, $login));
        $this->setMethod('post');
        $this->setAction('./login');

    }

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
    }


}

