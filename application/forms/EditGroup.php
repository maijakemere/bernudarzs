<?php

/**
 * Class Application_Form_EditGroup
 *
 * Form for editing existing group
 */

class Application_Form_EditGroup extends Zend_Form
{

    public function init()
    {
        //Set form name, send option and action
        $this->setName('editgroup');
        $this->setMethod('post');
        $this->setAction('../../update');

        $this->addElement('hidden','id', array(
            'label' => 'Id'
        ));

        // Add a group name element
        $this->addElement('text', 'name', array(
            'label'      => 'Grupas nosaukums:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array(
                    'validator' => 'StringLength',
                    'options'   => array(
                        'min'   => 2,
                        'max'   => 50,
                        'messages'  => array(
                            'stringLengthInvalid'   => 'Nosaukumam jābut starp %min% un %max% burtiem',
                            'stringLengthTooLong'   => 'Nosaukums nevar būt vairāk kā %max% burtus garš',
                            'stringLengthTooShort'  => 'Nosaukumam ir jābut vismaz %min% simbolus garam'
                        )
                    )
                ),
                array (
                    'validator' => 'NotEmpty' ,
                    'options'   => array(
                        'messages' => array(
                            'notEmptyInvalid'   => 'Ievadīts nepareizs datu tips',
                            'isEmpty'           => 'Lauks nedrīkst būt tukšs'
                        )
                    )
                )
            )
        ));

        // Add a group description element
        $this->addElement('textarea', 'description', array(
            'label'      => 'Apraksts:',
            'required'   => false,
            'filters'    => array('StringTrim')
        ));

        // Add the submit button
        $this->addElement('submit', 'submit', array(
            'ignore'   => true,
            'label'    => 'Labot grupu',
            'class'      => 'btn btn-primary'
        ));

    }

}

