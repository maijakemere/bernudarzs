<?php

/**
 * Class Application_Form_UploadImages
 *
 * Image upload form
 */

class Application_Form_UploadImages extends Zend_Form
{

    public function init()
    {
        //Add defaults
        $this->setMethod('post');
        $this->setAction('../../addpicture');

        //Add picture title
        $title = new Zend_Form_Element_Text('title', array(
            'label' => 'Title',
            'required' => false,
            'filters' => array(
                'StringTrim'
            ),
            'validators' => array(
                array('StringLength', false, array(3, 100))
            ),
        ));

        //Add file upload form
        $picture = new Zend_Form_Element_File('picture', array(
            'label' => 'Picture',
            'required' => true,
            'MaxFileSize' => 2097152, // 2097152 bytes = 2 megabytes !!! change to 5
            'validators' => array(
                array('Count', false, 1),
                array('Size', false, 2097152),
                array('Extension', false, 'gif,jpg,png')
            )
        ));

        $picture->setValueDisabled(true);

        //Add sublimt button
        $submit = new Zend_Form_Element_Submit('upload_picture', array(
            'label' => 'Upload the picture!'
        ));

        //Add all form elements
        $this->addElements(array(
            $title,
            $picture,
            $submit
        ));
    }



}

