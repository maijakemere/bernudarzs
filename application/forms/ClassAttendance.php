<?php

/**
 * Class Application_Form_ClassAttendance
 *
 * Form for creating class attendance
 */

class Application_Form_ClassAttendance extends Zend_Form
{

    public function init()
    {
        //Set form name, send option and action
        $this->setName('setattandance');
        $this->setMethod('post');
        $this->setAction('./attendance');

        // Gets data to display children list
        $groups = new Children_Model_GetChildrenInfo();
        $groupData = $groups->getGroupChildrenList();

        foreach ($groupData as $child){

            // Add a child attendance element
            $this->addElement('select', 'attendance', array(
                'label'      => 'Apmeklējums:',
                'required'   => true,
                'multiOptions'  => array(
                    '0'     => 'Apmeklēja',
                    '1'     => 'Neapmeklēja / attaisnots',
                    '2'     => 'Neapmeklēja / neattaisnots',
                    '3'     => 'Neapmeklēja / slimības dēļ'
                )
            ));

        }


    }


}

