<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    protected function _initAutoLoad()
    {
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $moduleLoader = new Zend_Application_Module_Autoloader(
            array(
                'namespace' => '',
                'basePath' => APPLICATION_PATH
            )
        );
        return $moduleLoader;
    }


    function _initViewHelpers()
    {
        $this->bootstrap('layout');
        $layout = $this->getResource('layout');
        $view = $layout->getView();

        $view->addHelperPath('ZendX/JQuery/View/Helper', 'ZendX_JQuery_View_Helper');

        ZendX_JQuery::enableView($view);
    }

    protected function _initNavigation()
    {
        $this->bootstrap('layout');
        $layout = $this->getResource('layout');
        $view = $layout->getView();

        $config = new Zend_Config_Xml(APPLICATION_PATH . '/configs/navigation.xml', 'nav');
        $navigation = new Zend_Navigation($config);
        $view->navigation($navigation);

        $childConfig = new Zend_Config_Xml(APPLICATION_PATH . '/configs/childnav.xml', 'nav');
        $childNavigation = new Zend_Navigation($childConfig);
        Zend_Registry::set('child',$childNavigation);

        $classConfig = new Zend_Config_Xml(APPLICATION_PATH . '/configs/classnav.xml', 'nav');
        $classNavigation = new Zend_Navigation($classConfig);
        Zend_Registry::set('class',$classNavigation);

        $employeeConfig = new Zend_Config_Xml(APPLICATION_PATH . '/configs/employeenav.xml', 'nav');
        $employeeNavigation = new Zend_Navigation($employeeConfig);
        Zend_Registry::set('child',$employeeNavigation);

        $structureConfig = new Zend_Config_Xml(APPLICATION_PATH . '/configs/structurenav.xml', 'nav');
        $structureNavigation = new Zend_Navigation($structureConfig);
        Zend_Registry::set('child',$structureNavigation);
    }
}

